function emsShowCookieNotice($)
{
    try {
        var bodytag = document.getElementsByTagName('body')[0];
        var div = document.createElement('div');
        div.setAttribute('class','bootstrap-wrapper');
        div.innerHTML =
            '<div id="ems-cookie-notice">' +
            '   <a id="ems-cn-close" href="#" class="close">&times;</a>' +
            '   <p>This website uses cookies. By further browsing you consent to such use.</p>' +
            '   <button id="ems-cn-ok-btn" type="button" class="btn btn-default">OK</button>' +
            '   <a href="/privacy/" target="_blank" id="ems-cn-learnmore-btn" role="button" class="btn btn-default">Learn More&nbsp;&raquo;</a>' +
            '</div>';

        bodytag.appendChild(div); // Adds the Cookie Law Banner just before the closing </body> tag
        // or
        //bodytag.insertBefore(div,bodytag.firstChild); // Adds the Cookie Law Banner just after the opening <body> tag

        $("#ems-cookie-notice").show();

        $("#ems-cn-close,#ems-cn-ok-btn,#ems-cn-learnmore-btn").click(function () {
            emsSetCookie("cookies_consent", 1, 30);
            $('#ems-cookie-notice').fadeOut("slow");
        });
    } catch (e) {
        console.log(e.message);
    }
}

function emsSetCookie(cookieName,value,exdays)
{
    try {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = encodeURI(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = cookieName + "=" + c_value + "; path=/";
    } catch (e) {
        console.log(e.message);
    }
}

function emsGetCookie(cookieName)
{
    try {
        var i,x,y,ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
            x = x.replace(/^\s+|\s+$/g,"");
            if (x == cookieName) {
                return decodeURI(y);
            }
        }
    } catch (e) {
        console.log(e.message);
    }
}

jQuery(document).ready(function ($) {
    var cookieConsent = emsGetCookie("cookies_consent");
    if (cookieConsent == "1") {
        return;
    }

    var emsVisitorLocationTryCount = 0;
    var emsVisitorLocationTimer = setInterval(function() {
        emsVisitorLocationTryCount++;
        //Get visitor location
        var visitorLocationJSON;
        if (typeof GetCookie === "function") {
            var visitorLocation = GetCookie("visitor_location");
            if (visitorLocation != null && visitorLocation != "" && visitorLocation != undefined) {
                try {
                    visitorLocationJSON = JSON.parse(visitorLocation);
                } catch (e) {
                    console.log(e.message);
                }
            }
        }

        try {
            if (emsVisitorLocationTryCount >= 5 || visitorLocationJSON != undefined) {
                clearInterval(emsVisitorLocationTimer);
                if (visitorLocationJSON == undefined || !visitorLocationJSON.hasOwnProperty("isEU")) {
                    $.ajax({
                        url: ajax_object_edc.edc_functions,
                        type: 'GET',
                        data: {action: 'emsGetVisitorLocation'},
                        cache: false,
                        dataType: 'json',
                        success: function (jsonResponse) {
                            try {
                                if (jsonResponse.status == 'success') {
                                    delete jsonResponse["status"];
                                    emsSetCookie("visitor_location", JSON.stringify(jsonResponse), 30);
                                    if (jsonResponse.isEU == true) {
                                        emsShowCookieNotice($);
                                    }
                                }
                                else if (jsonResponse.status == 'error' || jsonResponse.status == 'fail') {
                                    console.log(jsonResponse.message);
                                }
                            } catch (e) {
                                console.log(e.message);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr.responseText);
                        }
                    });
                }
                else if (visitorLocationJSON.isEU == true) {
                    emsShowCookieNotice($);
                }
            }
        } catch (e) {
            console.log(e.message);
        }
    });
});