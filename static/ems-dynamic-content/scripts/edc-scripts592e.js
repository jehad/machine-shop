function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function showAlertMessage($,alert,message)
{
    $(alert).find('span').html(message);
    $(alert).slideDown("fast");
}

function IsPositiveNumber($,val)
{
    if ($.isNumeric(val)) {
        return (val > 0);
    }

    return false;
}

function ctrlSetAlert($,control,state)
{
    var backgroundColor = '';
    var isCheckbox = $(control).is(':checkbox');
    var isButton = $(control).is(":button");
    if (state == true) {
        if (isCheckbox) {
            backgroundColor = '#da1010';
        }
        else {
            backgroundColor = '#f0dede';
        }
    }
    else {
        if (isCheckbox) {
            backgroundColor = '#000';
        }
    }

    if (isCheckbox) {
        $(control).closest("label").css('color', backgroundColor);
    }
    else if (isButton) {
        $(control).css('background-color', '');
    }
    else {
        $(control).css('background-color', backgroundColor);
    }
}

function notifyErrorMessage($,errorMsg,errorSubj)
{
    var formFunctionsURL = "";
    if (typeof ajax_object !== "undefined" && ajax_object.form_functions !== "undefined") {
        formFunctionsURL = ajax_object.form_functions;
    }
    else if (typeof ajax_object_edc !== "undefined" && typeof ajax_object_edc.edc_functions !== "undefined") {
        formFunctionsURL = ajax_object_edc.edc_functions;
    }
    else if (typeof popups_ajax_object !== "undefined" && typeof popups_ajax_object.edc_functions !== "undefined") {
        formFunctionsURL = popups_ajax_object.edc_functions;
    }

    if (formFunctionsURL != "") {
        $.ajax({
            url: formFunctionsURL,
            type: 'POST',
            data: {action: 'emsNotifyErrorMessage', errorMsg: errorMsg, errorSubj: errorSubj},
            cache: false,
            dataType: 'json',
            success: function (jsonResponse) {
                try {
                    if (jsonResponse != undefined && jsonResponse != null && jsonResponse.message != undefined) {
                        console.log(jsonResponse.message);
                    }
                } catch (e) {
                    console.log(e.message);
                }
            },
            error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
    }
}
