var edcDownloadSignupSilent = false;

//Facebook signin functions
function edcFacebookGetUserData() {
    try {
        FB.api('/me?fields=first_name,email', function(response) {
            jQuery("#signup-firstname").val(response.first_name);
            jQuery("#signup-email").val(response.email);
        });
    }
    catch(err) {
        console.log(err.message);
    }
}
window.fbAsyncInit = function() {
    try {
        //SDK loaded, initialize it
        FB.init({
            appId: "2016385041946738",
            xfbml: true,
            version: "v2.9"
        });

        //If name and email inputs are empty - check FB user session and refresh it
        if (document.getElementById("signup-firstname").value == "" || document.getElementById("signup-email").value == "") {
            FB.getLoginStatus(function (response) {
                if (response.status === "connected") {
                    //user is authorized
                    edcFacebookGetUserData();
                } else {
                    //user is not authorized
                }
            });
        }
    }
    catch(err) {
        console.log(err.message);
    }
};

//Google signin functions
var googleUser = {};
var startApp = function() {
    if (gapi != undefined) {
        try {
            gapi.load("auth2", function () {
                // Retrieve the singleton for the GoogleAuth library and set up the client.
                auth2 = gapi.auth2.init({
                    client_id: "405958871319-fke8io5ja9sjvg6n3r20clotpnnujtfq.apps.googleusercontent.com",
                    cookiepolicy: "single_host_origin"
                });


                auth2.attachClickHandler(document.getElementById('signup-google-btn'), {},
                    function (googleUser) {
                        jQuery("#signup-firstname").val(googleUser.getBasicProfile().getGivenName());
                        jQuery("#signup-email").val(googleUser.getBasicProfile().getEmail());
                        edcDownloadSignupSilent = true;
                        jQuery("#download-popup-btn").trigger("click");
                    }, function (error) {
                        console.log(JSON.stringify(error, undefined, 2));
                    });
            });
        }
        catch(err) {
            console.log(err.message);
        }
    }
};

function assignSocialLoginFunc($)
{
    try {
        //Load Facebook JavaScript SDK
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.com/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, "script", "facebook-jssdk"));

        //Facebook signin
        $("#signup-facebook-btn").click(function () {
            FB.login(function (response) {
                if (response.authResponse) {
                    edcDownloadSignupSilent = true;
                    edcFacebookGetUserData();//user just authorized your app
                    $("#download-popup-btn").trigger("click");
                }
            }, {scope: "email,public_profile", return_scopes: true});
        });

        //Google signin
        startApp();
    }
    catch(err) {
        console.log(err.message);
    }
}

function DownloadThankYouShowModal($)
{
    $("#download-thankyou-modal").modal();
    if (typeof ga === "function") {
        ga('send', 'event', 'download', 'ems-cad', '',  5.0);
    }
    else {
        $("#google-analytics-alert").show();
    }
}

function DoDownload()
{
    var downloadLink = document.getElementById("ems-download-link").href;
    document.location = downloadLink;
}


jQuery(document).ready(function ($) {
    //Try to get name and email from cookie
    if (typeof emsGetCookie === "function") {
        var userName = emsGetCookie("Name");
        var userEmail = emsGetCookie("Email");
        if (userName != '' && userEmail != '') {
            $("#signup-firstname").val(userName);
            $("#signup-email").val(userEmail);
        }
    }


    //Enable social login for non-EU users
    var visitorLocation = "";
    if (typeof emsGetCookie === "function") {
        visitorLocation = emsGetCookie("visitor_location");
    }
    var visitorLocationJSON;
    if (visitorLocation != null && visitorLocation != "" && visitorLocation != undefined) {
        try {
            visitorLocationJSON = JSON.parse(visitorLocation);
        } catch (e) {
            console.log(e.message);
        }
    }
    try {
        if (visitorLocationJSON == undefined || !visitorLocationJSON.hasOwnProperty("isEU")) {
            $.ajax({
                url: ajax_object_edc.edc_functions,
                type: "GET",
                data: {action: "emsGetVisitorLocation"},
                cache: false,
                dataType: "json",
                success: function (jsonResponse) {
                    try {
                        if (jsonResponse.status == "success") {
                            delete jsonResponse["status"];
                            if (typeof emsSetCookie === "function") {
                                emsSetCookie("visitor_location", JSON.stringify(jsonResponse), 30);
                            }
                            if (jsonResponse.isEU == true) {
                                $("#non-eu-block").hide();
                                $("#eu-block").show();
                            }
                            else {
                                $("#i-have-read-cbblock").hide();
                                $("#i-agree-receive-updates").prop("checked", true);
                                assignSocialLoginFunc($);
                            }
                        }
                        else if (jsonResponse.status == 'error' || jsonResponse.status == 'fail') {
                            console.log(jsonResponse.message);
                        }
                    } catch (e) {
                        console.log(e.message);
                    }
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }
        else if (visitorLocationJSON.isEU == true) {
            $("#non-eu-block").hide();
            $("#eu-block").show();
        }
        else if (visitorLocationJSON.isEU == false) {
            $("#i-have-read-cbblock").hide();
            $("#i-agree-receive-updates").prop("checked", true);
            assignSocialLoginFunc($);
        }
    } catch (e) {
        console.log(e.message);
    }


    $(".download-signup").click(function(){
        $("#download-signup-modal").modal();
    });

    $("#download-popup-btn").click(function(){
        var alertMessages = "";
        var crm_name = $("#signup-firstname").val();
        var crm_email = $("#signup-email").val();
        if (crm_name == "") {
            alertMessages += "First Name is required field.\n";
            ctrlSetAlert($,$("#signup-firstname"),true);
        }
        else if (crm_name.length < 2) {
            alertMessages += "Please enter a valid name.\n";
            ctrlSetAlert($,$("#signup-firstname"),true);
        }
        else {
            ctrlSetAlert($,$("#signup-firstname"),false);
        }


        if (crm_email == "") {
            alertMessages += "Email is required field.\n";
            ctrlSetAlert($,$("#signup-email"),true);
        }
        else if (!isValidEmailAddress(crm_email)) {
            alertMessages += "Please enter a valid email address.\n";
            ctrlSetAlert($,$("#signup-email"),true);
        }
        else {
            ctrlSetAlert($,$("#signup-email"),false);
        }


        if ($("#i-have-read").is(":visible") && !$("#i-have-read").is(":checked")) {
            alertMessages += "Accepting the Privacy Policy is required to continue.";
            ctrlSetAlert($,$("#i-have-read"),true);
        }
        else {
            ctrlSetAlert($,$("#i-have-read"),false);
        }


        if (alertMessages != "") {
            if (!edcDownloadSignupSilent) {
                alert(alertMessages);
            }
            return;
        }

        var crm_tag = "EMS - Downloaded";
        if ($("#i-agree-receive-updates").is(":checked")) {
            crm_tag += ",EMS - Newsletter";
        }

        //Save name and email to a cookie
        if (typeof emsSetCookie === "function") {
            var FiveYears = new Date(75, 1, 1, 1);
            emsSetCookie("Name", crm_name, FiveYears);
            emsSetCookie("Email", crm_email, FiveYears);
        }


        $("#signup-firstname,#signup-email,#download-popup-btn").prop("disabled",true);
        $("#processing-progressbar").css("width", "100%").attr("aria-valuenow", 100);
        $("#processing-progressbar > span").text("100%");
            $.ajax({
                url: ajax_object_edc.edc_functions,
                type: "POST",
                data: {
                    action: "emsAddToAC",
                    crm_tag: crm_tag,
                    crm_email: crm_email,
                    crm_name: crm_name
                },
                cache: false,
                dataType: "json",
                success: function (jsonResponse) {
                    try {
                        if (jsonResponse.status == 'error') {
                            console.log(jsonResponse.message);
                        }
                    } catch (e) {
                        console.log(e.message);
                    }
                    $("#download-signup-modal").modal("hide");
                    DownloadThankYouShowModal($);
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                    $("#download-signup-modal").modal("hide");
                    DownloadThankYouShowModal($);
                }
            });
        }
    );

    $("#download-thankyou-modal").on('shown.bs.modal', function () {
        var isMobile = window.matchMedia("only screen and (max-width: 760px)");
        if (!isMobile.matches) {
            setTimeout(DoDownload, 1000);
        }
    });
});

jQuery(window).load(function($){
    jQuery(".ems-multiple-slider").each(function () {
        var sliderID = "ems-slider-" + Math.random().toString(16).slice(2, 10);
        jQuery(this).attr("id", sliderID);
        var sliderNewClasses = "ems-multiple-slider carousel slide";
        if (jQuery("#" + sliderID).hasClass("ems-multiple-slider-opacity")) {
            sliderNewClasses += " ems-multiple-slider-opacity";
        }
        jQuery("#" + sliderID).removeClass().addClass(sliderNewClasses);
        jQuery("#" + sliderID).parent().addClass("bootstrap-wrapper");
        jQuery("#" + sliderID).find(".et_pb_slides").removeClass().addClass("carousel-inner");
        jQuery("#" + sliderID).find(".et_pb_slide").removeClass().addClass("item");


        jQuery("#" + sliderID).find(".carousel-inner .item").each(function () {
            var slideContent = jQuery(this).find(".et_pb_slide_content").html();
            jQuery(this).html("<div class='col-xs-4'>" + slideContent + "</div>");
        });

        jQuery("#" + sliderID).find(".carousel-inner .item").each(function () {
            var next = jQuery(this).next();
            if (!next.length) {
                next = jQuery(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo(jQuery(this));

            if (next.next().length > 0) {
                next.next().children(':first-child').clone().appendTo(jQuery(this));
            }
            else {
                jQuery(this).siblings(':first').children(':first-child').clone().appendTo(jQuery(this));
            }
        });

        jQuery("#" + sliderID).find(".et-pb-arrow-prev").attr("href", "#a").removeClass("et-pb-arrow-prev").
            addClass("arrow-prev").html('<span class="glyphicon glyphicon-menu-left"></span>').click(function () {
                jQuery("#" + sliderID).carousel("prev");
            });

        jQuery("#" + sliderID).find(".et-pb-arrow-next").attr("href", "#a").removeClass("et-pb-arrow-next").
            addClass("arrow-next").html('<span class="glyphicon glyphicon-menu-right"></span>').click(function () {
                jQuery("#" + sliderID).carousel("next");
            });

        jQuery("#" + sliderID).find(".item").first().addClass("active");
        jQuery("#" + sliderID).carousel();
    });
});
