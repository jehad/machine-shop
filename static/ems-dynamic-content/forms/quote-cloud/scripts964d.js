var fileData = new FormData();
var fileDataDND = new FormData();
var fileDataID = 0;

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function ctrlSetAlert($,control,state)
{
    var backgroundColor = '#fff';
    if (state == true) {
        backgroundColor = '#f0dede';
    }

    $(control).css('background-color',backgroundColor);
}

function showAlertMessage($,alert,message)
{
    $(alert).find('span').html(message);
    $(alert).slideDown("fast");
}
function setEmailInputErrState($,errorState)
{
    if (errorState) {
        ctrlSetAlert($,$("#email"),true);
    }
    else {
        ctrlSetAlert($,$("#email"),false);
        $("#email-alert").hide();
    }
}

function IsPositiveNumber($,val)
{
    if ($.isNumeric(val)) {
        return (val > 0);
    }

    return false;
}

function calculateFullwidth($)
{
    var dropdownsWidth;
    if ($(window).width() > 768) {
        var totalItemsWidth = 0;
        var rowWidth = $("#dynamic-fullwidth").width();

        //Material, Finishes, Quantity
        $("#dynamic-fullwidth > span, #dynamic-fullwidth > input, #dynamic-fullwidth > label, #dynamic-fullwidth > a").
            not(".material-finish-dropdown").each(function(){
                if ($(this).is(":visible")) {
                    totalItemsWidth += $(this).outerWidth(true);
                }
            });

        dropdownsWidth = (rowWidth-totalItemsWidth-32)/2;
        $(".material-finish-dropdown").innerWidth(dropdownsWidth);
        $("#quantity,#shipping-method").innerWidth($("#quantity").innerWidth()+5);
        var maxWidth = $("#emsform-quote-wrapper").width();
        var screenWidth = $(document).width();
        var finishesDDPosLeft = $("#finishes-desktop").offset().left;
        var finishesPosLeft = 0;
        var maxWidthMaterial = maxWidth;
        var maxWidthFinishes = maxWidth;
        if (maxWidth > 800) {
            maxWidthMaterial = 800;
            maxWidthFinishes = 700;
        }
        if (finishesDDPosLeft+maxWidthFinishes > screenWidth) {
            finishesPosLeft = screenWidth-finishesDDPosLeft-maxWidthFinishes-100;
        }

        $("#material-list-desktop").css('max-width',maxWidthMaterial+"px").find(".container").css('max-width',maxWidthMaterial+"px");
        $("#finishes-list-desktop").css('left',finishesPosLeft+"px").css('max-width',maxWidthFinishes+"px").
            find(".container").css('max-width',maxWidthFinishes+"px");


        //Apply alignment for inputs
        $("button[data-id='country']").closest("div.bootstrap-select").innerWidth(dropdownsWidth+2);
        $("#state").innerWidth(dropdownsWidth-2);
        $("#first-last-name,#email").innerWidth(dropdownsWidth);
        $("#label-state").innerWidth($("#label-finishes").innerWidth()+$("#tooltip-finishes").innerWidth());
        $("#label-shipping").innerWidth($("#label-quantity").innerWidth()+$("#tooltip-quantity").innerWidth());
    }
    else {
        $(".material-finish-dropdown,#quantity,#shipping-method,#state,#first-last-name,#email,#label-state,#label-shipping").css("width", "");
        $("button[data-id='country']").closest("div.bootstrap-select").innerWidth("");
    }
}

function checkStlObjAlert($)
{
    //If user uploads .STL or .OBJ, but did not select ABS under 3D printing in Material,
    //Reset material selection and show warning under materials: "STL and OBJ files are accepted for 3D Printing only."
    var showAlert = false;

    if ($("#upload-design-filename").data("files") != undefined) {
        var designFileExt = $("#upload-design-filename").data("files")[0].filename.split('.').pop().toLowerCase();
        if ($("#upload-design-filename").is(":visible") && (designFileExt == 'obj' || designFileExt == 'stl')) {
            if ($("#material").is(":visible") && $("#material").find(":selected").data("showalert") != "0") {
                showAlert = true;
            }
            else if ($("#material-desktop").is(":visible") && $("#material-desktop").data("showalert") != "0") {
                showAlert = true;
            }
        }
    }

    if (showAlert) {
        $(".material-alert").show();
        $("#material-desktop-title").text("- -");
        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $("#material").selectpicker("val", "");
    }
    else {
        $(".material-alert").hide();
    }
}

function checkShipping($)
{
    //Anything for USA (except Alaska and Hawaii) see Ground (Free) with 2nd day and next day options
    //For anything non USA or Alaska and Hawaii, show Economy and Rush options
    var selCountry = $("#country").find(":selected").text();
    var selState = $("#state").find(":selected").text();
    if (selCountry != "U.S.A." || selState == "AK" || selState == "HI") {
        $("#shipping-method .expshipping").show();
        $("#shipping-method .nexpshipping").hide();
        if ($("#shipping-method").find(":selected").hasClass("nexpshipping")) {
            $("#shipping-method").val("Economy");
        }
    }
    else {
        $("#shipping-method .expshipping").hide();
        $("#shipping-method .nexpshipping").show();
        if ($("#shipping-method").find(":selected").hasClass("expshipping")) {
            $("#shipping-method").val("Ground (Free)");
        }
    }
}

function getFileExt(fileName)
{
    var regExpExt = /(?:\.([^.]+))?$/;
    var fileExt = regExpExt.exec(fileName)[1];
    return fileExt;
}
function checkAllowedToUpload(file,uploadSender,allowedExt)
{
    var errorMsg = '';
    var fileExt = getFileExt(file.name);
    if (file.size > 1024 * 1024 * 64) {
        errorMsg = "File too big: " + file.name + " exceeds our maximum allowed file size.";
    }
    else if (file.size < 100) {
        errorMsg = "File too small: " + file.name + " exceeds our minimum allowed file size.";
    }
    else if (file.name.length > 255) {
        errorMsg = "File name should not exceed 255 characters. Please rename file.";
    }
    else if (allowedExt.indexOf(fileExt.toLowerCase()) == -1) {
        if (uploadSender == "design") {
            errorMsg = "Uploaded file type not accepted. Please upload a PDF file.";
        }
        else if (uploadSender == "photos") {
            errorMsg = "Uploaded file type not accepted. Please upload a PDF, JPG, PNG, GIF or BMP file.";
        }
        else {
            errorMsg = fileExt + " file extension is not allowed to upload.";
        }
    }

    return errorMsg;
}

function setSelectedCountry($,jsonLocation)
{
    $("#country").selectpicker("val", jsonLocation.countryCode);
    $("#state").val(jsonLocation.region);

    if ($("#country").selectpicker("val") == "") {
        $("#country").selectpicker("val","US");
        $("#state").val("NJ");
    }
    $("#country").trigger("change");
}

function showCadControls($)
{
    $("#general-controls").slideDown("fast",function(){
        calculateFullwidth($);
        checkStlObjAlert($);
    });

    $("#ems-cad-controls").hide();
}

jQuery(document).ready(function ($) {
    $("#emsform-quote-wrapper").css("display","block");
    $('[data-toggle="tooltip"]').tooltip();

    $("#comments").attr("placeholder", $("#comments").data("placeholder-def"));
    $("#tooltip-comments").attr("data-original-title", $("#comments").data("placeholder-def"));


    $(".panel-collapse").on("shown.bs.collapse", function(){
        var windowScrollTop = $(window).scrollTop();
        var elementTop = $(this).closest(".panel").offset().top-20;
        if (windowScrollTop > elementTop) {
            $('html, body').animate({
                scrollTop: elementTop
            }, 200);
        }
    });

    $(window).resize(function(){
        calculateFullwidth($);
    });


    //Don't allow to drag&drop/upload any files outside the drop area
    window.addEventListener("dragover",function(e){
        e = e || event;
        e.preventDefault();
    },false);
    window.addEventListener("drop",function(e){
        e = e || event;
        e.preventDefault();
    },false);


    var dropZone = document.getElementById('drop-zone');
    var startUpload = function(files) {
        // Initial upload should only allow for 1 file to be uploaded.
        // Any one requiring multiple files (like images) would have an upload section in their fields. 
        var currentFile = files[0];
        var fileExtension = currentFile.name.substr((currentFile.name.lastIndexOf('.')+1)).toLowerCase();
        var allowedToUpload = true;
        var fileTypesGraphic = ["pdf", "jpg", "jpeg", "png", "gif", "bmp"];
        var fileTypesText = ["txt", "doc", "docx", "odt", "wpd", "wks", "wps", "rtf", "tex"];
        var fileTypesCAD = ["igs", "iges", "stp", "step", "sldprt", "dwg", "dxf", "amf", "cgr", "eprt", "hcg", "hsf",
            "ifc", "prt", "prtdot", "sat", "sldlfp", "vda", "wrl", "xaml", "x_b", "x_t", "3dxml", "stl", "obj"];
        if (fileExtension == "ems") {
            $("#emsform-quote-page").data("type","ems");
            $("#ems-cad-controls").slideDown("fast");
            $("#general-controls").hide();
            if ($("#ems-cad-image").attr("src") == "") {
                $("#ems-cad-image").attr("src", $("#ems-cad-image").data("src"));
            }
        }
        else if (fileTypesGraphic.indexOf(fileExtension) >= 0) {
            $("#emsform-quote-page").data("type","image");
            $("#dimensions-group").addClass("row-spacer").insertAfter("#photos-text-descr");
            $("#tooltip-dimensions").attr("data-original-title", $("#tooltip-dimensions").data("standart-title"));
            $("#part-details-comments,#part-details-design-file").hide();
            $("#tooltip-dimensions,#general-controls-spacer,#dimensions-group").show();
            $("#image-text-controls-block").slideDown("fast");
            showCadControls($);
        }
        else if (fileTypesText.indexOf(fileExtension) >= 0) {
            $("#emsform-quote-page").data("type","text");
            $("button[data-id='country']").closest("div.bootstrap-select")

            $("#dimensions-group").hide();
            $("#image-text-controls-block,#part-details-comments,#part-details-design-file").slideUp("fast");
            showCadControls($);
        }
        else if (fileTypesCAD.indexOf(fileExtension) >= 0) {
            $("#emsform-quote-page").data("type","cad");
            $("#image-text-controls-block").slideUp("fast");

            $("#tooltip-dimensions,#general-controls-spacer,#part-details-comments,#part-details-design-file").show();
            $("#tooltip-dimensions").attr("data-original-title",
                $("#tooltip-dimensions").data("cad-title")+$("#tooltip-dimensions").data("standart-title"));
            $("#dimensions-group").detach().appendTo("#cad-dimensions-block").removeClass("row-spacer").show();
            $("#part-details-design-file,#part-details-comments").slideDown("fast");
            showCadControls($);
        }
        else {
            $("#emsform-quote-page").data("type","");
            allowedToUpload = false;
        }


        if (allowedToUpload) {
            $("#upload-file-name").show().find("span").text(currentFile.name);
            $("#upload-file-description").hide();
            fileDataDND = currentFile;
        }
        else {
            $("#upload-file-description").text(fileExtension+" is not allowed to upload").show();
            $("#ems-cad-controls,#general-controls,#upload-file-name").slideUp("fast");
        }
    };

    dropZone.ondrop = function(e) {
        e.preventDefault();
        this.className = 'upload-drop-zone';
        startUpload(e.dataTransfer.files);
    };

    dropZone.ondragover = function() {
        this.className = 'upload-drop-zone drop';
        return false;
    };

    dropZone.ondragleave = function() {
        this.className = 'upload-drop-zone';
        return false;
    };

    $("#btn-upload-new-file").click(function(){
        $("#upload-dnd").trigger("click");
    });

    $("#upload-dnd").on("change", function() {
        startUpload(this.files);
    });


    //@@@@
    //1. перенести и изменить код загрузки файлов additional file (CAD) и additional images (Image)
    //2. Убрать upload.php
    //3. Перенести всю загрузку файлов в submit.php
    //Done
    $("#upload-additional-cad-button").click(function(){
        $("#upload-additional-cad").trigger("click");
    });

    $("#upload-additional-image-button").click(function(){
        $("#upload-additional-image").trigger("click");
    });

    $("#upload-additional-cad").on("change", function() {
        var allowedExt = ["pdf"];
        var errorMsg = "";
        var uploadedIDs = [];
        var uploadedFileName = "";
        for (var i = 0; i < this.files.length; i++) {
            var currentErrorMsg = checkAllowedToUpload(this.files[i], "design", allowedExt);
            if (currentErrorMsg == "") {
                uploadedIDs.push(fileDataID);
                fileData.append(fileDataID, this.files[i]);
                fileDataID++;
                uploadedFileName = this.files[i].name;
            }
            else {
                errorMsg += currentErrorMsg+"<br>";
            }
        }

        if (uploadedIDs.length > 1) {
            uploadedFileName = uploadedIDs.length+" files selected";
        }
        $("#upload-additional-cad-filename").data("file-id",uploadedIDs).text(uploadedFileName);

        if (errorMsg != "") {
            showAlertMessage($,$("#upload-additional-cad-alert"),errorMsg);
        }
        else {
            $("#upload-additional-alert").hide();
        }
    });

    $("#upload-additional-image").on("change", function() {
        var allowedExt = ["pdf", "jpg", "png", "gif", "bmp"];
        var errorMsg = "";
        var uploadedIDs = [];
        var uploadedFileName = "";
        for (var i = 0; i < this.files.length; i++) {
            var currentErrorMsg = checkAllowedToUpload(this.files[i], "photos", allowedExt);
            if (currentErrorMsg == "") {
                uploadedIDs.push(fileDataID);
                fileData.append(fileDataID, this.files[i]);
                fileDataID++;
                uploadedFileName = this.files[i].name;
            }
            else {
                errorMsg += currentErrorMsg+"<br>";
            }
        }

        if (uploadedIDs.length > 1) {
            uploadedFileName = uploadedIDs.length+" files selected";
        }
        $("#upload-additional-image-filename").data("file-id",uploadedIDs).text(uploadedFileName);

        if (errorMsg != "") {
            showAlertMessage($,$("#upload-photos-alert"),errorMsg);
        }
        else {
            $("#upload-photos-alert").hide();
        }


        /*var errorMsg = "";
        var allowedExt = ["pdf", "jpg", "png", "gif", "bmp"];
        for (var i = 0; i < this.files.length; ++i) {
            errorMsg = checkAllowedToUpload(this.files[i],"photos",allowedExt);
            if (errorMsg != "") {
                break;
            }
        }
        if (errorMsg != "") {
            showAlertMessage($,$("#upload-photos-alert"),errorMsg);
            $(this).val("");
            return;
        }

        var displayFileName = '';
        if (this.files.length == 1) {
            displayFileName = this.files[0].name;
        }
        else {
            displayFileName = this.files.length+" files selected";
        }
        $("#upload-additional-image-filename").html(displayFileName);
        $(".material-alert,#upload-photos-alert").hide();*/
    });



    $("#dimensions").on("change paste keydown", function(e) {
        var key = String.fromCharCode(e.which).toLowerCase();
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188, 190]) !== -1 || // Allow: backspace, delete, tab, escape, enter
            ((e.keyCode == 45 || e.keyCode == 65 || e.keyCode == 86 || e.keyCode == 88 || e.keyCode == 67) &&
                (e.ctrlKey === true || e.metaKey === true)) ||// Allow: Ctrl+A,Ctrl+C,Ctrl+X,Ctrl+V, Command+A
            (e.keyCode >= 35 && e.keyCode <= 40) || // Allow: home, end, left, right, down, up
            key == 'x' && $(this).val().split("x").length-1 < 2) { //Allow x character
            return;
        }

        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#material").on("change", function (e) {
        checkStlObjAlert($);
    });



    //Material and Finish multiview dropdown events and functions
    $("#material-list-desktop .dd-item").click(function(){
        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $(this).addClass("dd-active-item");
        var selectedMaterialName = $(this).text();
        var selectedMaterialGroup = $(this).closest("div.material-group").data("group");
        var selectedMaterialGroupTitle = $(this).closest("div.material-group").data("group-title");
        $("#material-desktop-title").text(selectedMaterialGroupTitle+" - "+selectedMaterialName);

        if ($(this).data("showalert") == undefined) {
            $("#material-desktop").data("showalert", "1");
        }
        else {
            $("#material-desktop").data("showalert", $(this).data("showalert"));
        }
        checkStlObjAlert($);

        var mobileMaterialValue = $("#material optgroup[data-group='"+selectedMaterialGroup+"'] option:contains('"+selectedMaterialName+"')").attr("value");
        $("#material").selectpicker("val", mobileMaterialValue).trigger("change");
    });
    //Avoid finishes list dropdown menu close on click inside
    $(document).on("click", "#finishes-list-desktop", function (e) {
        e.stopPropagation();
    });
    $("#finishes-list-desktop .dd-item").click(function(){
        if ($(this).hasClass("as-manufactured")) {
            $("#finishes-list-desktop .dd-item").removeClass("dd-active-item");
        }
        else {
            $("#finishes-list-desktop .as-manufactured").removeClass("dd-active-item");
        }

        if ($(this).hasClass("dd-active-item")) {
            $(this).removeClass("dd-active-item");
        }
        else {
            $(this).addClass("dd-active-item");
        }

        var selectedFinishes = "";
        var selectedFinishesCount = 0;
        $("#finishes-list-desktop .dd-active-item").each(function(){
            selectedFinishes += $(this).text()+", ";
            selectedFinishesCount++;
        });
        selectedFinishes = selectedFinishes.replace(/,\s*$/, ""); //Remove last comma and space
        $("#finishes-list-desktop").data("selected",selectedFinishes);
        if (selectedFinishesCount > 2) {
            $("#finishes-desktop-title").text(selectedFinishesCount+" items selected");
        }
        else if (selectedFinishesCount == 0) {
            $("#finishes-desktop-title").text(" - - ");
        }
        else {
            $("#finishes-desktop-title").text(selectedFinishes);
        }
    });
    $("#material").on("change", function() {
        var selectedMaterialName = $(this).find(":selected").text();
        var selectedMaterialValue = $(this).find(":selected").val();
        var selectedMaterialGroup = $(this).find(":selected").parent("optgroup").data('group');
        $(this).find(":selected").attr("title",selectedMaterialValue);

        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $("#material-desktop-title").text(selectedMaterialValue);
        $("#material-list-desktop div[data-group='"+selectedMaterialGroup+"'] div.dd-item:contains('"+selectedMaterialName+"')").addClass("dd-active-item");
    });
    $("#finishes").on("changed.bs.select", function (e, clickedIndex) {
        if ($($("#finishes option")[clickedIndex]).hasClass("as-manufactured")) {
            $("#finishes").selectpicker("deselectAll").selectpicker("val", ["As Manufactured"]);
        }
        else {
            var selectedValues = $("#finishes").selectpicker("val");
            var asManufacturedIndex = selectedValues.indexOf("As Manufactured");
            if (asManufacturedIndex >= 0) {
                selectedValues.splice(asManufacturedIndex, 1);
                $("#finishes").selectpicker("val", selectedValues);
            }
        }
    });


    //Check if email address is valid and email confirm match email
    $("#email").focusout(function(){
        if (!isValidEmailAddress($(this).val()) || $(this).val() == "") {
            showAlertMessage($,$("#email-alert"),"Please enter a valid email address.");
            setEmailInputErrState($,true);
        }
        else {
            setEmailInputErrState($,false);
        }
    });

    $("#country").on("change", function (e) {
        var selCountry = $(this).find(":selected").text();
        if (selCountry == "U.S.A.") {
            $("#state").prop("disabled", false);
            $("#state option[value='N/A']").remove();
        }
        else {
            $('#state').prop('disabled', true).append($('<option/>', {
                value: 'N/A',
                text : 'N/A'
            })).val("N/A");
        }
    });
    $("#state,#country").on("change", function (e) {
        checkShipping($);
    });



    $("#btn-submit").click(function(){
        var alertMessages = '';
        var formData = new FormData();
        var inputsData = $("#emsform-quote-page").find("textarea, select, input[type=text], input[type=number], input[type=email]").serializeArray();

        //Check required fields
        var requiredMissing = '';
        var requiredMissingCount = 0;
        $(".required:visible").each(function() {
            if ($(this).val() == "" || $(this).val() == null) {
                ctrlSetAlert($,"#"+$(this).attr("id"),true);
                requiredMissing += $(this).data("required")+", ";
                requiredMissingCount++;
            }
            else {
                ctrlSetAlert($,"#"+$(this).attr("id"),false);
            }
        });

        //Check Material dropdown
        if ($("#material").is(":visible")) {
            if ($("#material").selectpicker("val") == null) {
                ctrlSetAlert($, $("#material").parent().find("button.dropdown-toggle"), true);
                requiredMissing += "Material, ";
                requiredMissingCount++;
            }
            else {
                ctrlSetAlert($, $("#material").parent().find("button.dropdown-toggle"), false);
            }
        }
        else if ($("#material-desktop").is(":visible")) {
            if ($("#material-desktop-title").text() == '- -') {
                ctrlSetAlert($, $("#material-desktop"), true);
                requiredMissing += "Material, ";
                requiredMissingCount++;
            }
            else {
                ctrlSetAlert($, $("#material-desktop"), false);
            }
        }

        //Check if design file selected
        if ($("#ftype-cad").is(":checked") && !$("#upload-design-cad-filename").is(":visible") && !$("#upload-design-pdf").is(":visible")) {
            $("#label-design-file").css("color","red");
            requiredMissing += "Design File, ";
            requiredMissingCount++;
        }
        else {
            $("#label-design-file").css("color","");
        }

        //Check if photos file selected or written design description is not empty
        if ($("#ftype-image-text").is(":checked") && $("#upload-photos-filename").data("files") == undefined &&
            $("#written-design-description").val() == "") {
            $("#label-photos-file").css("color","red");
            ctrlSetAlert($, $("#written-design-description"), true);
            alertMessages += "Provide Image(s), Written Description, or both.\n";
        }
        else {
            $("#label-photos-file").css("color","");
            ctrlSetAlert($, $("#written-design-description"), false);
        }

        requiredMissing = requiredMissing.replace(/,\s*$/, ""); //Remove last comma and space
        var replacement = ' and';  //Replace last comma to 'and'
        requiredMissing = requiredMissing.replace(/,([^,]*)$/,replacement+'$1');

        if (requiredMissingCount == 1) {
            alertMessages += "Required field missing: " + requiredMissing + " is a required field.\n";
        }
        else if (requiredMissingCount > 1) {
            alertMessages += "Required fields:\n" + requiredMissing + "\n";
        }

        //Length, width, height
        if ($("#emsform-quote-page").data("type") == "image" || $("#emsform-quote-page").data("type") == "cad") {
            var dimensions = $("#dimensions").val();
            var dimensionsArr = dimensions.split("x");

            //Check dimensions
            var dimensionsAlert = false;
            for (var i = 0; i < dimensionsArr.length; i++) {
                dimensionsArr[i] = dimensionsArr[i].replace(",", ".");
                if (!IsPositiveNumber($,dimensionsArr[i])) {
                    ctrlSetAlert($,"#dimensions",true);
                    dimensionsAlert = true;
                }
                else {
                    ctrlSetAlert($,"#dimensions",false);
                }
            }
            if (dimensionsAlert) {
                alertMessages += "Dimensions: Please enter a non-zero positive number.\n";
            }

            //Check part size
            var dimL = dimensionsArr[0] * 1.0;
            var dimW = dimensionsArr[1] * 1.0;
            var dimH = dimensionsArr[2] * 1.0;
            var max_dim = dimL;
            if (dimW > max_dim)
                max_dim = dimW;
            if (dimH > max_dim)
                max_dim = dimH;

            var mid_dim = (dimL + dimW + dimH) / 3.0;
            if (($("#dimensions-inches").is(":checked") && (max_dim > 47.5 || mid_dim > 23.5)) ||
                ($("#dimensions-mm").is(":checked") && (max_dim > 1206.5 || mid_dim > 596.9))) {
                alertMessages += "Part size too large.\n";
                dimensionsAlert = true;
            }
            else {
                inputsData.push(
                    {name: 'length', value: dimL.toString()},
                    {name: 'width', value: dimW.toString()},
                    {name: 'height', value: dimH.toString()}
                );
            }

            if (dimensionsAlert) {
                ctrlSetAlert($,"#dimensions",true);
            }
            else {
                ctrlSetAlert($,"#dimensions",false);
            }
        }

        //Check quantity
        if ($("#ftype-cad").is(":checked") || $("#ftype-image-text").is(":checked")) {
            if (!IsPositiveNumber($,$("#quantity").val())) {
                ctrlSetAlert($,"#quantity",true);
                alertMessages += "Quantity: Please enter a non-zero positive number.\n";
            }
            else {
                ctrlSetAlert($,"#quantity",false);
            }
        }

        if (!$("#i-agree-terms").is(":checked")) {
            alertMessages += "Agreement with Terms of Use and Order Policies is required.\n";
        }

        //Display error messages if present
        if (alertMessages != "") {
            alert(alertMessages);
            return;
        }


        //Add finishes (mobile and desktop) manually
        if ($('#finishes').is(":visible") && $('#finishes').selectpicker('val') != null) {
            inputsData.push({name: 'finishes', value: $('#finishes').selectpicker('val').join(", ")});
        }
        else if ($('#finishes-desktop').is(":visible") && $("#finishes-list-desktop").data("selected") != undefined &&
            $("#finishes-list-desktop").data("selected") != "") {
            inputsData.push({name: 'finishes', value: $("#finishes-list-desktop").data("selected")});
        }

        //Add material (desktop) manually
        if ($("#material-desktop").is(":visible")) {
            inputsData.push({name: 'material', value: $("#material-desktop-title").text()});
        }

        if ($("#dimensions-inches").is(":checked")) {
            inputsData.push({name: 'dimensions-units', value: 'Inches'});
        }
        else {
            inputsData.push({name: 'dimensions-units', value: 'MM'});
        }


        //Add files to a formData array and submit the form
        formData.append("file_0", fileDataDND);
        var fileDataCount = 1;

        if ($("#emsform-quote-page").data("type") == "image") {
            var fileData = $("#upload-additional-image")[0].files;
            for (var i = 0; i < fileData.length; i++, fileDataCount++) {
                formData.append("file_" + fileDataCount, fileData[i]);
            }
        }
        else if ($("#emsform-quote-page").data("type") == "cad") {
            var fileData = $("#upload-additional-cad")[0].files;
            for (var i = 0; i < fileData.length; i++, fileDataCount++) {
                formData.append("file_" + fileDataCount, fileData[i]);
            }
        }

        $.each(inputsData,function(key,input){
            formData.append(input.name,input.value);
        });


        $("#btn-submit").prop("disabled", true).text("Uploading...");
        $.ajax({
            url: ajax_object.form_quote_submit,
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            context: this,
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // For handling the progress of the upload
                    myXhr.upload.addEventListener('progress', function(e) {
                        if (e.lengthComputable) {
                            var uploadedPercent = Math.round(100 * e.loaded / e.total);
                            $("#btn-submit").prop("disabled", true).text("Uploading... "+uploadedPercent+"%");
                        }
                    } , false);
                }
                return myXhr;
            },
            success: function(jsonResponse) {
                try {
                    if (jsonResponse.status == 'success') {
                        $("#request-submitted-modal").modal();
                        location.hash = 'submitted';

                        if (typeof ga === "function") {
                            var d = new Date();
                            var n = d.getTime();
                            ga('send', 'event', 'quote', 'place', 'web-' + n, 68); //BM: Post to google analytics
                        }
                        else {
                            $("#google-analytics-alert").show();
                        }

                        if (typeof SetCookie === "function") {
                            SetCookie('Name', $("#first-last-name").val());
                            SetCookie('Email', $("#email").val());
                        }
                        $("#btn-submit").prop("disabled", false).text("Submit");
                    }
                    else if (jsonResponse.status == 'error') {
                        $("#btn-submit").prop("disabled", false).text("Submit");
                        $(this).prop('disabled', false);
                        alert(jsonResponse.message);
                    }
                } catch(e) {
                    $("#btn-submit").prop("disabled", false).text("Submit");
                    $(this).prop('disabled', false);
                    alert(e.message);
                }
            },
            error: function(xhr) {
                $("#btn-submit").prop("disabled", false).text("Submit");
                $(this).prop('disabled', false);
                alert(xhr.responseText);
            }
        });
    });

    $("#request-submitted-modal").on("hide.bs.modal", function(e) {
        window.location.href = "/";
    });

    $("#next-part").click(function(){
        var currentPageURL = "//" + location.host + location.pathname;
        var newPageURL = currentPageURL + "?cemail=" + $("#email").val() + "&cname=" + $("#first-last-name").val();
        window.location.href = newPageURL;
    });


    if (typeof GetCookie === "function") {
        if ($("#first-last-name").val() == "") {
            $("#first-last-name").val(GetCookie("Name"));
        }
        if ($("#email").val() == "") {
            $("#email").val(GetCookie("Email"));
        }
    }


    //Get visitor location
    var emsVisitorLocationTryCount = 0;
    var emsVisitorLocationTimer = setInterval(function(){
        emsVisitorLocationTryCount++;
        var visitorLocationJSON;
        if (typeof GetCookie === "function") {
            var visitorLocation = GetCookie("visitor_location");
            if (visitorLocation != null && visitorLocation != "" && visitorLocation != undefined) {
                try {
                    visitorLocationJSON = JSON.parse(visitorLocation);
                } catch (e) {
                    console.log(e.message);
                }
            }
        }

        if (emsVisitorLocationTryCount >= 5 || visitorLocationJSON != undefined) {
            clearInterval(emsVisitorLocationTimer);
            if (visitorLocationJSON == undefined) {
                $.ajax({
                    url: ajax_object.form_functions,
                    type: 'GET',
                    data: {action: 'emsGetVisitorLocation'},
                    cache: false,
                    dataType: 'json',
                    success: function (jsonResponse) {
                        try {
                            if (jsonResponse.status == 'success') {
                                setSelectedCountry($, jsonResponse);
                                if (typeof SetCookieExp === "function") {
                                    delete jsonResponse["status"];
                                    SetCookieExp("visitor_location", JSON.stringify(jsonResponse), 30);
                                }
                            }
                            else if (jsonResponse.status == 'error' || jsonResponse.status == 'fail') {
                                console.log(jsonResponse.message);
                            }
                        } catch (e) {
                            console.log(e.message);
                        }
                    },
                    error: function (xhr) {
                        console.log(xhr.responseText);
                    }
                });
            }
            else {
                setSelectedCountry($, visitorLocationJSON);
            }
        }
        emsVisitorLocationTryCount++;
    }, 1000);
});