jQuery(document).ready(function ($) {
    $(".download-guide-modal").on("show.bs.modal", function () {
        $(this).closest(".download-guide-modal-wrapper").detach().prependTo("#page-container");
    }).on("shown.bs.modal", function () {
        try {
            var guideModalElement = $(this).closest(".download-guide-modal");
            var tagGA = $(guideModalElement).data("ganame")+" Show";
            try {
                if (ga.q) {
                    console.log("GA not working");
                    notifyErrorMessage($,"GA not working (shown modal)","EMS Guide popup: "+tagGA);
                }
                else {
                    ga("send", "event", "guide-popup", tagGA);
                }
            } catch(e) {
                console.log(e.message);
                notifyErrorMessage($,e.message,"EMS Guide Popup: "+tagGA);
            }
        } catch (e) {
            console.log(e.message);
        }
    });

    $(".download-guide-modal .guide-download-btn").click(function(){
        var guideModalElement = $(this).closest(".download-guide-modal");
        var firstName = $(guideModalElement).find(".guide-first-name").val();
        var email = $(guideModalElement).find(".guide-email").val();

        if (email == "test@test.test") {
            notifyErrorMessage($,"GA TEST","EMS Guide Popup TEST");
        }

        if (email == "") {
            alert("Email is a required field");
            ctrlSetAlert($,$(guideModalElement).find(".guide-email"),true);
            return false;
        }
        else if (!isValidEmailAddress(email)) {
            alert("Please enter a valid email address");
            ctrlSetAlert($,$(guideModalElement).find(".guide-email"),true);
            return false;
        }
        else {
            ctrlSetAlert($,$(guideModalElement).find(".guide-email"),false);
        }

        var tagAC = $(guideModalElement).data("actag");
        if ($(guideModalElement).find(".agree-receive-updates").is(":checked")) {
            tagAC += ",EMS - Newsletter";
        }

        var tagGA = $(guideModalElement).data("ganame")+" Download";
        try {
            if (ga.q) {
                console.log("GA not working");
                notifyErrorMessage($,"GA not working (download click)","EMS Guide popup: "+tagGA);
            }
            else {
                ga("send", "event", "guide-popup", tagGA);
            }
        } catch(e) {
            console.log(e.message);
            notifyErrorMessage($,e.message,"EMS Guide Popup: "+tagGA);
        }

        $.ajax({
            url: popups_ajax_object.edc_functions,
            type: "POST",
            data: {
                action: "emsAddToAC",
                crm_tag: tagAC,
                crm_email: email,
                crm_name: firstName
            },
            cache: false,
            dataType: "json",
            success: function (jsonResponse) {
                try {
                    if (jsonResponse.status == 'error') {
                        console.log(jsonResponse.message);
                    }
                } catch (e) {
                    console.log(e.message);
                }
            },
            error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
        $(guideModalElement).modal("hide");
    });
});