var calculatingPriceMessages = [
    "Your price is being calculated"
];
var alertMessages = '';
var requiredMissing = '';
var requiredMissingCount = 0;
var requestLogID;
var groupsMultiSelectAllowed = ["finish-mechanical","finish-other","finish-none"];
var groupsMultiSelectNotAllowed = ["finish-electroplate","finish-powder-coat","finish-chemical-film","finish-anodizing"];
var validRALColors = {
    "1000":"Green Beige",   "1001":"Beige",         "1002":"Sand Yellow",   "1003":"Signal Yellow",
    "1004":"Golden Yellow", "1005":"Honey Yellow",  "1006":"Maize Yellow",  "1007":"Daffodil Yellow",
    "1011":"Brown Beige",   "1012":"Lemon Yellow",  "1013":"Oyster White",  "1014":"Ivory",
    "1015":"Light Ivory",   "1016":"Sulfur Yellow", "1017":"Saffron Yellow","1018":"Zinc Yellow",
    "1019":"Grey Beige",    "1020":"Olive Yellow",  "1021":"Rap Yellow",    "1023":"Traffic Yellow",
    "1024":"Ochre Yellow",  "1027":"Curry",         "1028":"Melon Yellow",  "1032":"Broom Yellow",
    "1033":"Dahlia Yellow", "1034":"Pastel Yellow", "2000":"Yellow Orange", "2001":"Red Orange",
    "2002":"Vermilion",     "2003":"Pastel Orange", "2004":"Pure Orange",   "2008":"Bright Red Orange",
    "2009":"Traffic Orange","2010":"Signal Orange", "2011":"Deep Orange",   "2012":"Salmon Orange",
    "3000":"Flame Red",     "3001":"Signal Red",    "3002":"Carmine Red",   "3003":"Ruby Red",
    "3004":"Purple Red",    "3005":"Wine Red",      "3007":"Black Red",     "3009":"Oxide Red",
    "3011":"Brown Red",     "3012":"Beige Red",     "3013":"Tomato Red",    "3014":"Antique Pink",
    "3015":"Light Pink",    "3016":"CoRed",         "3017":"Rose",          "3018":"Strawberry Red",
    "3020":"Traffic Red",   "3022":"Salmon Pink",   "3027":"Raspberry Red", "3031":"Orient Red",
    "4001":"Red Lilac",     "4002":"Red Violet",    "4003":"Heather Violet","4004":"Claret Violet",
    "4005":"Blue Lilac",    "4006":"Traffic Purple","4007":"Purple Violet", "4008":"Signal Violet",
    "4009":"Pastel Violet", "5000":"Violet Blue",   "5001":"Green Blue",    "5002":"Ultramarine Blue",
    "5003":"Sapphire Blue", "5004":"Black Blue",    "5005":"Signal Blue",   "5007":"Brilliant Blue",
    "5008":"Grey Blue",     "5009":"Azure Blue",    "5010":"Gentian Blue",  "5011":"Steel Blue",
    "5012":"Light Blue",    "5013":"Cobalt Blue",   "5014":"Pigeon Blue",   "5015":"Sky Blue",
    "5017":"Traffic Blue",  "5018":"Turquoise Blue","5019":"Capri Blue",    "5020":"Ocean Blue",
    "5021":"Water Blue",    "5022":"Night Blue",    "5024":"Pastel Blue",   "6000":"Patina Green",
    "6001":"Emerald Green", "6002":"Leaf Green",    "6003":"Olive Green",   "6004":"Blue Green",
    "6005":"Moss Green",    "6006":"Grey Olive",    "6007":"Bottle Green",  "6008":"Brown Green",
    "6009":"Fir Green",     "6010":"Grass Green",   "6011":"Reseda Green",  "6012":"Black Green",
    "6013":"Reed Green",    "6014":"Yellow Olive",  "6015":"Black Olive",   "6016":"Turquoise Green",
    "6017":"May Green",     "6018":"Yellow Green",  "6019":"Pastel Green",  "6020":"Chrome Green",
    "6021":"Pale Green",    "6022":"Olive Drab",    "6024":"Traffic Green", "6025":"Fern Green",
    "6026":"Opal Green",    "6027":"Light Green",   "6028":"Pine Green",    "6029":"Mint Green",
    "6032":"Signal Green",  "7000":"Squirrel Grey", "7001":"Silver Grey",   "7002":"Olive Grey",
    "7003":"Moss Grey",     "7004":"Signal Grey",   "7005":"Mouse Grey",    "7006":"Beige Grey",
    "7008":"Khaki Grey",    "7009":"Green Grey",    "7010":"Tarpaulin Grey","7011":"Iron Grey",
    "7012":"Basalt Grey",   "7013":"Brown Grey",    "7015":"Slate Grey",    "7016":"Anthracite Grey",
    "7021":"Black Grey",    "7022":"Umbra Grey",    "7023":"Concrete Grey", "7024":"Graphite Grey",
    "7026":"Granite Grey",  "7030":"Stone Grey",    "7031":"Blue Grey",     "7032":"Pebble Grey",
    "7033":"Cement Grey",   "7034":"Yellow Grey",   "7035":"Light Grey",    "7036":"Platinum Grey",
    "7037":"Dusty Grey",    "7038":"Agate Grey",    "7039":"Quartz Grey",   "7040":"Window Grey",
    "7042":"Traffic Grey A","7043":"Traffic Grey B","7044":"Silk Grey",     "7045":"Telegrey 1",
    "7046":"Telegrey 2",    "7047":"Telegrey 4",    "8000":"Green Brown",   "8001":"Ocher Brown",
    "8002":"Signal Brown",  "8003":"Clay Brown",    "8004":"Copper Brown",  "8007":"Fawn Brown",
    "8008":"Olive Brown",   "8011":"Nut Brown",     "8012":"Red Brown",     "8014":"Sepia Brown",
    "8015":"Chestnut Brown","8016":"Mahogany Brown","8017":"Chocolate Brown","8019":"Grey Brown",
    "8022":"Black Brown",   "8023":"Orange Brown",  "8024":"Beige Brown",   "8025":"Pale Brown",
    "8028":"Terra Brown",   "9001":"Cream",         "9002":"Grey White",    "9003":"Signal White",
    "9004":"Signal Black",  "9005":"Jet Black",     "9006":"Aluminum",      "9007":"Grey Aluminum",
    "9010":"Pure White",    "9011":"Graphite Black","9016":"Traffic White", "9017":"Traffic Black",
    "9018":"Papyrus White"
};


function calculateFullwidth($)
{
    if ($(window).width() > 768) {
        var maxWidth = $("#emsform-instant-quote-page1").width();
        var screenWidth = $(document).width();
        var finishesDDPosLeft = $("#finishes-desktop").offset().left;
        var finishesPosLeft = 0;
        var maxWidthMaterial = maxWidth;
        var maxWidthFinishes = maxWidth;
        if (maxWidth > 800) {
            maxWidthMaterial = 800;
            maxWidthFinishes = 700;
        }
        if (finishesDDPosLeft+maxWidthFinishes > screenWidth) {
            finishesPosLeft = screenWidth-finishesDDPosLeft-maxWidthFinishes-100;
        }

        $("#material-list-desktop").css('max-width',maxWidthMaterial+"px").find(".container").css('max-width',maxWidthMaterial+"px");
        $("#finishes-list-desktop").css('left',finishesPosLeft+"px").css('max-width',maxWidthFinishes+"px").
            find(".container").css('max-width',maxWidthFinishes+"px");
    }
}

function switchToPage3($)
{
    $("#emsform-instant-quote-page2").fadeOut("slow", function(){
        $("#page3-summary-block").html("<div class='row'>" +
        "<div class='col-xs-8'>" + $("#processing-file-name").text() + "</div>" +
        "<div class='col-xs-4 text-right'>" + $("#subtotal-value").text() + "</div>" +
        "</div>" +
        "<div class='row'>" +
        "<div class='col-xs-8'>" + $("#pmaterial").val() + "</div>" +
        "<div class='col-xs-4 text-right'>Qty: " + $("#pquantity").val() + "</div>" +
        "</div>" +
        "<div class='row'>" +
        "<div class='col-xs-12'>" + $("#pfinish").val() + "</div>" +
        "</div>");//Add <hr> if panel will have more than 1 item

        $("#page3-subtotal-value").text($("#subtotal-value").text());
        $("#page3-shipping-value").text($("#shipping-value").text());
        $("#page3-tax-value").text($("#tax-value").text());
        $("#page3-total-value").text($("#total-price").text());

        var shippingSelectedID = $("#shipping-select option:selected").data("methodid");
        $("#shipping-radio-"+shippingSelectedID).prop("checked", true);

        $("#trusted-by").hide();
        $("#emsform-instant-quote-page3").fadeIn("slow");
        history.pushState({screen: "3"}, null);
    });
}

function page3UpdateBlocks($)
{
    if ($(window).width() >= 768) {
        $("#page3-steps").insertBefore("#page3-shipping-summary");
    }
    else {
        $("#page3-shipping-summary").insertBefore("#page3-steps");
    }
}

function setFormMaterialParam($)
{
    var material = '';
    var materialID = 0;
    if ($("#material").is(":visible")) {
        if ($("#material").selectpicker("val") == null) {
            ctrlSetAlert($, $("#material").parent().find("button.dropdown-toggle"), true);
            requiredMissing += "Material, ";
            requiredMissingCount++;
            return true;
        }
        else {
            material = $("#material").selectpicker("val").join();
            material = material.replace(/\*$/, "");   //Remove last Asterisk from Material (Steel A36*)
            materialID = $("#material").data("materialid");
            ctrlSetAlert($, $("#material").parent().find("button.dropdown-toggle"), false);
        }
    }
    else if ($("#material-desktop").is(":visible")) {
        if ($("#material-desktop-title").text() == '- -') {
            ctrlSetAlert($, $("#material-desktop"), true);
            requiredMissing += "Material, ";
            requiredMissingCount++;
            return true;
        }
        else {
            material = $("#material-desktop-title").text();
            materialID = $("#material-desktop-title").data("materialid");
            ctrlSetAlert($, $("#material-desktop"), false);
        }
    }

    if (material != '' && materialID != 0) {
        $("#pmaterial").val(material);
        $("#pmaterialid").val(materialID);
        return true;
    }

    return false;
}

function setFormFinishesParam($)
{
    var finish = '';
    var finishIDs = '';
    if ($("#finishes").is(":visible")) {
        $("#finishes").find("option:selected").each(function(){
            finish += $(this).val()+", ";
            finishIDs += $(this).data("finishid")+", ";
        });
    }
    else if ($("#finishes-desktop").is(":visible")) {
        $("#finishes-list-desktop").find("div.dd-active-item").each(function(){
            finish += $(this).text()+", ";
            finishIDs += $(this).data("finishid")+", ";
        });
    }

    finish = finish.replace(/,\s*$/, "");
    finishIDs = finishIDs.replace(/,\s*$/, "");

    if (finish != '' && finishIDs != '') {
        $("#pfinish").val(finish);
        $("#pfinishid").val(finishIDs);
        return true;
    }

    return false;
}

function getQuantity($,elemID)
{
    var quantity = $(elemID).val();
    if (!IsPositiveNumber($,quantity)) {
        ctrlSetAlert($,elemID,true);
        alertMessages += "Quantity: Please enter a non-zero positive number.\n";
        quantity = '';
    }
    else {
        ctrlSetAlert($,elemID,false);
    }

    return quantity;
}

function getThickness($,elemID)
{
    var thickness = $(elemID).val();
    if (!IsPositiveNumber($,thickness)) {
        ctrlSetAlert($,elemID,true);
        alertMessages += "Thickness: Please enter a non-zero positive number.\n";
        thickness = '';
    }
    else {
        ctrlSetAlert($,elemID,false);
    }

    return thickness;
}

function convertMmToInch(value)
{
    //There is a downside that values like 1.5 will give "1.50" as the output
    var inches = +(value * 0.0393701).toFixed(2);

    // Note the plus sign that drops any "extra" zeroes at the end.
    // It changes the result (which is a string) into a number again (think "0 + foo"),
    // which means that it uses only as many digits as necessary.
    return inches.toString();
}

function calculateTotalPrice($)
{
    var subtotalValue = $("#table-lead-time").find("input.pretty-radio:checked").data("price");
    var taxValue = $("#table-lead-time").find("input.pretty-radio:checked").data("tax");
    var totalPrice = 0;

    $("#subtotal-value").data("value",subtotalValue).text("$"+subtotalValue);
    if ($("#subtotal-value").data("value") != undefined) {
        totalPrice += Number(subtotalValue);
    }
    if (taxValue != undefined && taxValue != 0) {
        totalPrice += Number(taxValue);
        $("#tax-value").text("$"+taxValue);
    }
    else {
        $("#tax-value").text("- -");
    }

    if ($("#shipping-value").data("value") != undefined) {
        totalPrice += Number($("#shipping-value").data("value"));
    }
    $("#total-price").text("$"+totalPrice.toFixed(2));
}

function makeEqualHeight($)
{
    if ($("#panel-quote-processing").is(":visible")) {
        $(".col-equal-height").css("height", "");
        var docWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if (docWidth >= 1200) {
            var heights = $(".col-equal-height").map(function () {
                    var colHeight = $(this).height();
                    if ($(this).attr("id") == "col-place-order") {
                        var colElementsHeight = $("#table-lead-time").height()+$("#place-order-block").height();
                        if (colElementsHeight > colHeight) {
                            colHeight = colElementsHeight;
                        }
                    }
                    return colHeight;
                }).get(),
                maxHeight = Math.max.apply(null, heights);
            $(".col-equal-height").css("height", maxHeight);
        }
    }
}

function checkAlerts($)
{
    //If user uploads .STL or .OBJ, but did not select ABS under 3D printing in Material,
    //Reset material selection and show warning under materials: "STL and OBJ files are accepted for 3D Printing only."
    var showAlert = false;

    if ($("#design-filename").text() != undefined) {
        var designFileExt = $("#design-filename").text().split('.').pop().toLowerCase();
        if (designFileExt == 'obj' || designFileExt == 'stl') {
            if ($("#material").is(":visible") && $("#material").find(":selected").data("showalert") != "0") {
                showAlert = true;
            }
            else if ($("#material-desktop").is(":visible") && $("#material-list-desktop .dd-active-item").length != 0 &&
                $("#material-list-desktop .dd-active-item").data("showalert") != "0") {
                showAlert = true;
            }
        }
    }

    if (showAlert) {
        $(".material-alert").show();
        $("#material-desktop-title").text("- -");
        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $("#material").selectpicker("val", "");
    }
    else {
        $(".material-alert").hide();
    }


    //Show warning alert when acrylic and polycarbonate is selected that it may become opaque during machining
    var selectedMaterialName = "";
    if ($("#material").is(":visible")) {
        selectedMaterialName = $("#material").find(":selected").text();
    }
    else if ($("#material-desktop").is(":visible")) {
        selectedMaterialName = $("#material-list-desktop .dd-active-item").text();
    }

    var opaqueAlertArr = ["acrylic","polycarbonate"];
    if (opaqueAlertArr.indexOf(selectedMaterialName.toLowerCase()) == -1) {
        $(".opaque-alert").slideUp("fast");
    }
    else {
        $(".opaque-alert span").text("Clear stock materials such as "+selectedMaterialName+" may become opaque during the machining process.");
        $(".opaque-alert").slideDown("fast");
    }
}

function setSelectedCountry($,jsonLocation)
{
    $("#select-country").selectpicker("val", jsonLocation.countryCode);
    $("#select-state").val(jsonLocation.region);

    if ($("#select-country").selectpicker("val") == "") {
        $("#select-country").selectpicker("val","US");
        $("#select-state").val("NJ");
    }
    $("#select-country").trigger("change");
}

function logResponse($,requestProcessed,requestStartTime,jsonResponse)
{
    var publicMsg = "";
    var errorMsg = "";
    var internalMsg = "";
    var priceTimeMsg = "";
    if (jsonResponse != null) {
        var priceTimeCombos = jsonResponse.combos;
        for (var i = 0; i < priceTimeCombos.length; i++) {
            var mfgPriceTotal = Number(priceTimeCombos[i].MfgPrice);
            var mfgDays = priceTimeCombos[i].MfgDays;
            if (priceTimeCombos[i].Tax != undefined) {
                mfgPriceTotal += Number(priceTimeCombos[i].Tax);
            }
            mfgPriceTotal = parseFloat(mfgPriceTotal).toFixed(2);
            priceTimeMsg += "{"+mfgDays+","+mfgPriceTotal+"},";
        }
        priceTimeMsg = "["+priceTimeMsg.replace(/,\s*$/, "")+"]";   //Remove last comma and format

        publicMsg = jsonResponse.notes;
        errorMsg = jsonResponse.errors;
        internalMsg = jsonResponse.internal_message;
    }
    else if (!requestProcessed) {
        errorMsg = $("#processing-dimensions").text() + ". " + $("#processing-error-heading").text("File Cannot be Quoted");
        publicMsg = $("#processing-error-reason").text();
    }

    var requestEndTime = new Date();
    var elapsedTimeSeconds = Math.round((requestEndTime - requestStartTime) / 1000);

    $.ajax({
        url: ajax_object.form_instant_quote_functions,
        type: "GET",
        data: {
            action: "logResponse",
            type: "processed",
            request_log_id: requestLogID,
            job_num: $("#pjobnum").val(),
            price_time: priceTimeMsg,
            error_msg: errorMsg,
            public_msg: publicMsg,
            internal_msg: internalMsg,
            time_elapsed: elapsedTimeSeconds
        },
        cache: false,
        dataType: "json",
        success: function(jsonResponse) {
        },
        error: function(xhr) {
            alert(xhr.responseText);
        }
    });
}

function processServerResponse($,responseText,status,requestStartTime)
{
    $("#btn-new-file-quote").show().prop("disabled",false);
    $("#btn-place-order,#btn-modify-quote").prop("disabled",false);
    $("#processing-please-wait").hide();

    var jsonResponse = JSON.parse(responseText);
    if (status == '200' && jsonResponse.status == "success" && $.isNumeric(jsonResponse.price)) {
        var price = parseFloat(jsonResponse.price).toFixed(2);
        var priceTimeCombos = jsonResponse.combos;
        var tolerance = jsonResponse.specifications.Tolerance;
        var quantityValue = $("#pquantity").val();
        $("#material-desktop-mobile-block").detach().appendTo("#processing-material-block");
        $("#finish-desktop-mobile-block").detach().appendTo("#processing-finish-block");

        //Set tolerance
        $("#processing-tolerance option[data-mm='" + tolerance +"']").attr("selected","selected");

        //Set Price/Time combos
        var priceTimeTableStr = "";
        var lowestPrice = -1;
        var lowestPriceRadioID = "";
        for (var i = 0; i < priceTimeCombos.length; i++) {
            var mfgPriceTotal = parseFloat(priceTimeCombos[i].MfgPrice).toFixed(2);
            var mfgPriceEach = (mfgPriceTotal / parseFloat(quantityValue)).toFixed(2);
            var taxValue = 0;
            var mfgDays = priceTimeCombos[i].MfgDays;
            if (priceTimeCombos[i].Tax != undefined) {
                taxValue = parseFloat(priceTimeCombos[i].Tax).toFixed(2);
            }

            var priceRadioID = "rbtn-pricetime-"+mfgDays;
            if (lowestPrice == -1 || parseFloat(mfgPriceTotal) < parseFloat(lowestPrice)) {
                lowestPrice = mfgPriceTotal;
                lowestPriceRadioID = priceRadioID;
            }

            priceTimeTableStr += "<tr>"+
            "<td class='col-radiobutton'>"+
            "<input type='radio' id='"+priceRadioID+
            "' class='pretty-radio pricetime-radio' "+
            "data-price='"+mfgPriceTotal+
            "' data-tax='"+taxValue+"' name='pricetime-radio'></td>"+
            "<td class='text-center'>"+mfgDays+"</td>"+
            "<td class='text-right'>$"+mfgPriceEach+"</td>"+
            "<td class='text-right'>$"+mfgPriceTotal+"</td>"+
            "</tr>";
        }

        //Show with price when cheapest/only option exceeds $10k.
        if (Number(lowestPrice) > 10000) {
            $("#call-bestpricing-note").show();
        }
        else {
            $("#call-bestpricing-note").hide();
        }

        $("#table-lead-time > tbody").html(priceTimeTableStr);
        $("#table-lead-time").slideDown("fast");

        $("#table-lead-time input.pricetime-radio").click(function(){
            calculateTotalPrice($);
        });
        $("#table-lead-time #"+lowestPriceRadioID).prop("checked",true).trigger("click");

        //Show dimensions
        var jsonDimensions = jsonResponse.part;
        var dimensionsStr = convertMmToInch(jsonDimensions.X)+'" x '+convertMmToInch(jsonDimensions.Y)+'" x '+
            convertMmToInch(jsonDimensions.Z)+'"';
        $("#processing-dimensions").text(dimensionsStr);

        //Add shipping methods radiobuttons for 2nd and 3rd screen
        var shippingMethodsSelect = '';
        var shippingMethodsRadio = '';
        var firstShippingCost = -1;
        for (var index = 0; index < jsonResponse.shipping.length; ++index) {
            var shippingID = jsonResponse.shipping[index].MethodID;
            var methodName = jsonResponse.shipping[index].MethodName;
            var shippingCost = jsonResponse.shipping[index].Cost.toFixed(2);
            var shippingCostText = "$"+shippingCost;
            if (shippingCost == 0) {
                shippingCostText = "FREE";
            }
            shippingMethodsSelect += '<option data-cost="'+shippingCost+
            '" data-methodid="'+shippingID+'">'+methodName+' | '+shippingCostText+'</option>';
            shippingMethodsRadio += '<label class="radio-inline">'+'<input type="radio" class="pretty-radio" name="shipping-radio" data-cost="'+
            shippingCost+'" id="shipping-radio-'+shippingID+'">&nbsp;'+methodName+' | '+shippingCostText+'</label><br>\n';
            if (firstShippingCost == -1) {
                firstShippingCost = shippingCost;
            }
        }
        $("#shipping-select").html(shippingMethodsSelect);

        $("#subtotal-value").data("value",price).text("$" + price);
        var firstShippingCostText = "$"+firstShippingCost;
        if (firstShippingCost == 0) {
            firstShippingCostText = "FREE";
        }
        $("#shipping-value").data("value",firstShippingCost).text(firstShippingCostText);

        //Set data for 3rd screen
        $("#panel-shipping-speed > div.panel-body").html(shippingMethodsRadio);

        //Show errors, warnings, notes
        var errors = "";
        var warnings = "";
        var notes = "";
        var processingHint = "";
        if (jsonResponse.errors != "") {
            errors = jsonResponse.errors.replace(/\|/g, "<br>");
            processingHint = errors+". Pricing might be understated. Please: remove the feature and resubmit -OR- request a manual quote -OR- place your order (you will be contacted for approval if a price change applies).<br>";
        }
        if (jsonResponse.notes != "") {
            notes = jsonResponse.notes.replace(/\|/g, "<br>");
            processingHint += "Note: "+notes+"<br>";
        }
        if (jsonResponse.warnings != "") {
            warnings = jsonResponse.warnings.replace(/\|/g, "<br>");
            processingHint += warnings+"<br>";
        }

        if (processingHint == "") {
            $("#processing-hint-block").hide();
        }
        else {
            $("#processing-hint-block").show();
        }
        $("#processing-hint").html(processingHint);

        // @@@For temp.use - that information is for those testing the form
        // After the system has been debugged out and the form is open for public use,
        // we will hide that area from public view.
        var processingInternalNotes = "";
        if (jsonResponse.sw_price != "") {
            processingInternalNotes += "SW price: $"+jsonResponse.sw_price;
        }
        if (jsonResponse.internal_message != "") {
            processingInternalNotes += "<br>"+jsonResponse.internal_message;
        }


        //Show images that come in "sw_images" parameter of the server response
        var swImagesArr = ["Default", "Top", "Bottom", "Front", "Back", "Left", "Right"];
        for (i = 0; i < swImagesArr.length; i++) {
            var swImage = swImagesArr[i];
            var swImageLower = swImage.toLowerCase();
            if (jsonResponse.sw_images[swImage] != "") {
                $("#sw-images-block [data-img-toggle='#sw-image-"+swImageLower+"']").show();
                $("#sw-images-block #sw-image-"+swImageLower).data("file",jsonResponse.sw_images[swImage]).data("processed","0");
            }
            else {
                $("#sw-images-block [data-img-toggle='#sw-image-"+swImageLower+"']").hide();
            }
        }
        $("#sw-images-block").show();

        if (processingInternalNotes == "") {
            $("#processing-dbginfo-block").hide();
        }
        else {
            $("#processing-dbginfo-block").show();
        }
        $("#processing-dbginfo-hint").html(processingInternalNotes);


        calculateTotalPrice($);
        $("#processing-select-params").show();
        makeEqualHeight($);
        logResponse($,true,requestStartTime,jsonResponse);//@@@@1
    }
    else {
        $("#processing-place-order").hide();
        $("#processing-manual-quote").show();
        $("#processing-dimensions").text("Service Unavailable");
        $("#processing-error-heading").text("File Requires Manual Quotation");
        $("#processing-error-reason").html(jsonResponse.errors.replace(/\|/g, "<br>"));
        $("#processing-manual-quote-button").show();
        makeEqualHeight($);
        logResponse($,false,requestStartTime,null);//@@@@1
    }
}

jQuery(document).ready(function ($) {
    $("#form-background").slideDown("slow");
    $("#trusted-by").slideDown("fast");
    $('[data-toggle="tooltip"]').tooltip();
    history.pushState({screen: "1"}, null);

    $("#page3-phone").mask("(999) 999-9999? x99999",{autoclear: false});
    $("#page3-cvv").mask("9999",{autoclear: false, placeholder:" "});

    var creditCardsArr = ["amex", "discover", "mastercard", "visa"];
    var cleaveCCNumber = new Cleave('#card-number', {
        creditCard: true,
        onCreditCardTypeChanged: function (type) {
            creditCardType = type;
            $("#img-cc-block > .img-cc:not(.img-cc-active)").addClass("img-cc-active");
            if (creditCardsArr.indexOf(type) >= 0) {
                $("#img-cc-block > .img-cc-active").removeClass("img-cc-active");
                $("#img-cc-"+type).addClass("img-cc-active");
            }
        }
    });


    //Material and Finish multiview dropdown events and functions
    $(window).resize(function(){
        calculateFullwidth($);
        makeEqualHeight($);
        page3UpdateBlocks($);
    });
    calculateFullwidth($);
    page3UpdateBlocks($);

    //Maintain Browser History When Using AJAX
    $(window).on('popstate', function(event) {
        if (event.originalEvent.state != null && event.originalEvent.state != "null" && event.originalEvent.state.screen != null) {
            if (event.originalEvent.state.screen == 1) {
                $("#emsform-instant-quote-page2").fadeOut("slow", function(){
                    $("#form-background").addClass("ems-img-background");
                    $("#material-desktop-mobile-block").detach().appendTo("#init-material-block");
                    $("#finish-desktop-mobile-block").detach().appendTo("#init-finish-block");
                    $("#emsform-instant-quote-page1").show();
                });
            }
            else if (event.originalEvent.state.screen == 2) {
                if ($("#emsform-instant-quote-page3").is(":visible")) {
                    $("#emsform-instant-quote-page3").fadeOut("slow", function () {
                        $("#emsform-instant-quote-page2").fadeIn("slow");
                        $("#trusted-by").show();
                    });
                }
                else if ($("#emsform-instant-quote-page1").is(":visible")) {
                    $("#emsform-instant-quote-page1").fadeOut("slow", function () {
                        $("#form-background").removeClass("ems-img-background");
                        $("#material-desktop-mobile-block").detach().appendTo("#processing-material-block");
                        $("#finish-desktop-mobile-block").detach().appendTo("#processing-finish-block");
                        $("#emsform-instant-quote-page2").fadeIn("slow");
                    });
                }
            }
            else if (event.originalEvent.state.screen == 3) {
                $("#emsform-instant-quote-page2").fadeOut("slow", function(){
                    $("#emsform-instant-quote-page3").fadeIn("slow");
                    $("#trusted-by").hide();
                });
            }
        }
    });

    $(".payment-tab").click(function(){
        var tabFromLi = $("#payment-nav-tabs").find("li.active");
        var tabFrom = $(tabFromLi).find("a.payment-tab").data("toggle-tab");
        var tabToLi = $(this).closest("li");
        var tabTo = $(this).data("toggle-tab");
        $(tabFromLi).removeClass("active");
        $(tabToLi).addClass("active");
        $(tabFrom).fadeOut("fast").promise().done(function() {
            $(tabTo).fadeIn("fast");
        });
    });

    $("#material-list-desktop .dd-item").click(function(){
        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $(this).addClass("dd-active-item");
        var selectedMaterialID = $(this).data("materialid");
        var selectedMaterialName = $(this).text();
        $("#material-desktop-title").text(selectedMaterialName).data("materialid",selectedMaterialID);
        $("#material").selectpicker("val", "- -");
        checkAlerts($);
    });
    //Avoid finishes list dropdown menu close on click inside
    $(document).on('click', '#finishes-list-desktop', function (e) {
        e.stopPropagation();
    });
    $("#finishes-list-desktop .dd-item").click(function(){
        if ($(this).hasClass("as-manufactured")) {
            $("#finishes-list-desktop .dd-item").removeClass("dd-active-item");
        }
        else {
            $("#finishes-list-desktop .as-manufactured").removeClass("dd-active-item");
        }

        var itemPreviouslySelected = false;
        if ($(this).hasClass("dd-active-item")) {
            itemPreviouslySelected = true;
        }

        //Only allow one selection per section
        if (groupsMultiSelectAllowed.indexOf($(this).closest(".material-group").data("group")) == -1) {
            for (var i = 0; i < groupsMultiSelectNotAllowed.length; i++) {
                $("#finishes-list-desktop [data-group='" + groupsMultiSelectNotAllowed[i] + "']").find(".dd-item").removeClass("dd-active-item");
            }
        }

        if (itemPreviouslySelected) {
            $(this).removeClass("dd-active-item");
        }
        else {
            $(this).addClass("dd-active-item");
        }


        var selectedFinishes = "";
        var selectedFinishesCount = 0;
        $("#finishes-list-desktop .dd-active-item").each(function(){
            selectedFinishes += $(this).text()+", ";
            selectedFinishesCount++;
        });
        selectedFinishes = selectedFinishes.replace(/,\s*$/, ""); //Remove last comma and space
        $("#finishes-list-desktop").data("selected",selectedFinishes);
        if (selectedFinishesCount > 2) {
            $("#finishes-desktop-title").text(selectedFinishesCount+" items selected");
        }
        else if (selectedFinishesCount == 0) {
            $("#finishes-desktop-title").text(" - - ");
        }
        else {
            $("#finishes-desktop-title").text(selectedFinishes);
        }
    });
    $("#material").on("change", function() {
        var selectedMaterialName = $(this).find(":selected").val();
        var selectedMaterialID = $(this).find(":selected").data("materialid");
        $(this).find(":selected").attr("title",selectedMaterialName);
        $(this).data("materialid",selectedMaterialID);
        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $("#material-desktop-title").text("- -");
        checkAlerts($);
    });
    $("#finishes").on("changed.bs.select", function (e, clickedIndex, isSelected, previousValue) {
        if ($($("#finishes option")[clickedIndex]).hasClass("as-manufactured")) {
            $("#finishes").selectpicker("deselectAll").selectpicker("val", ["As Manufactured"]);
        }
        else {
            var itemsMultiSelectAllowed = [];
            var itemsMultiSelectNotAllowed = [];
            var i;
            for (i = 0; i < groupsMultiSelectNotAllowed.length; i++) {
                $("#finishes optgroup[data-group='"+groupsMultiSelectNotAllowed[i]+"']").children().each(function () {
                    itemsMultiSelectNotAllowed.push(this.value);
                });
            }
            for (i = 0; i < groupsMultiSelectAllowed.length; i++) {
                $("#finishes optgroup[data-group='"+groupsMultiSelectAllowed[i]+"']").children().each(function () {
                    itemsMultiSelectAllowed.push(this.value);
                });
            }

            //Only allow one selection per section
            var selectedValues = $("#finishes").selectpicker("val");
            var clickedValue = $("#finishes option")[clickedIndex].value;
            if (itemsMultiSelectAllowed.indexOf(clickedValue) == -1 && selectedValues != null) {
                for (i = 0; i < itemsMultiSelectNotAllowed.length; i++) {
                    //removeFrom selectedValues each not allowed value
                    var itemIndex = selectedValues.indexOf(itemsMultiSelectNotAllowed[i]);
                    if (itemIndex !== -1) {
                        selectedValues.splice(itemIndex, 1);
                    }
                }
            }

            if (selectedValues != null) {
                selectedValues.push(clickedValue);

                //Remove as manufactured from selected items
                var asManufacturedIndex = selectedValues.indexOf("As Manufactured");
                if (asManufacturedIndex >= 0) {
                    selectedValues.splice(asManufacturedIndex, 1);
                }

                //Deselect previously selected value (if it was selected)
                if (selectedValues.indexOf(clickedValue) >= 0 && !isSelected) {
                    selectedValues.splice(selectedValues.indexOf(clickedValue), 1);
                }
                $("#finishes").val(selectedValues).selectpicker("render");
            }
        }
    });
    //Show update button if any input on 2nd screen will be changed
    $("#material-list-desktop .dd-item, #finishes-list-desktop .dd-item, " +
        "#processing-thickness-inches, #processing-thickness-mm").click(function(){
        if ($("#emsform-instant-quote-page2").is(":visible")) {
            $("#btn-modify-quote").show();
            $("#btn-place-order").prop("disabled",true);
        }
    });
    $("#material,#finishes,#processing-tolerance").on("change", function() {
        if ($("#emsform-instant-quote-page2").is(":visible")) {
            $("#btn-modify-quote").show();
            $("#btn-place-order").prop("disabled",true);
        }
    });
    $("#processing-quantity,#processing-thickness").on("keypress", function(){
        $("#btn-modify-quote").show();
        $("#btn-place-order").prop("disabled",true);
    });



    $("#select-country,#page3-country").on("change", function (e) {
        var selCountry = $(this).find(":selected").text();
        var selStateAssoc = $(this).data("selstate");
        if (selCountry == "U.S.A.") {
            $(selStateAssoc).prop("disabled", false);
            $(selStateAssoc+" option[value='N/A']").remove();
        }
        else {
            $(selStateAssoc).prop("disabled", true).append($("<option/>", {
                value: "N/A",
                text : "N/A"
            })).val("N/A");
        }
    });


    $("#btn-upload-design").click(function(){
        $("#browse-for-file").trigger("click");
    });

    $("#browse-for-file").on("change", function() {
        var errorMsg = '';
        var file = this.files[0];
        var regExpExt = /(?:\.([^.]+))?$/;
        var fileExt = regExpExt.exec(file.name)[1];
        var allowedExt = ["igs", "iges", "stp", "step", "sldprt", "dwg", "dxf", "amf", "cgr", "eprt", "hcg", "hsf", "ifc",
                "prt", "prtdot", "sat", "sldlfp", "vda", "wrl", "xaml", "x_b", "x_t", "3dxml", "stl", "obj"];

        /*if (file.size > 1024*1024*64) {
            errorMsg = "File too big: "+file.name+" exceeds our maximum allowed file size.";
        }
        else if (file.size < 1024) {
            errorMsg = "File too small: "+file.name+" exceeds our minimum allowed file size.";
        }
        else if (file.name.length > 255) {
            errorMsg = "File name should not exceed 255 characters. Please rename file.";
        }
        else */if (allowedExt.indexOf(fileExt.toLowerCase()) == -1) {
            errorMsg = fileExt+' file extension is not allowed to upload';
        }

        if (errorMsg != '') {
            $("#design-filename").html('');
            $("#upload-design-alert").html(errorMsg);
            $("#browse-for-file").val('');
        }
        else {
            $("#upload-design-alert").html('');
            $("#design-filename").text(file.name);

            //Add thickness field if DXF uploaded
            var designFileExt = file.name.split('.').pop().toLowerCase();
            if (designFileExt == 'dxf') {
                $("#col-thickness").slideDown("slow");
                $("#processing-thickness-block").show();
            }
            else {
                $("#col-thickness").slideUp("fast");
                $("#processing-thickness-block").hide();
            }
        }

        checkAlerts($);
    });

    $("#btn-get-quote").on("click", function() {
        alertMessages = '';
        requiredMissing = '';
        requiredMissingCount = 0;

        $("#emsform-instant-quote-page1 .required:visible").each(function() {
            if ($(this).val() == "") {
                ctrlSetAlert($, "#" + $(this).attr("id"), true);
                requiredMissing += $(this).data("required") + ", ";
                requiredMissingCount++;
            }
            else {
                ctrlSetAlert($, "#" + $(this).attr("id"), false);
            }
        });

        //Check Material dropdown and set Material name to form
        if (!setFormMaterialParam($)) {
            alert("Error: Cannot get material");
            return;
        }

        //Set Finishes selected values to form
        if (!setFormFinishesParam($)) {
            alert("Error: Cannot get finish");
            return;
        }


        //Check if design file selected
        if ($("#design-filename").html() == '') {
            $("#label-design-file").css("color","red");
            requiredMissing += "Design File, ";
            requiredMissingCount++;
        }
        else {
            $("#label-design-file").css("color","");
        }


        if ($("#select-state").val() == null) {
            ctrlSetAlert($,"#select-state",true);
            requiredMissing += "State, ";
            requiredMissingCount++;
        }
        else {
            ctrlSetAlert($,"#select-state",false);
        }

        requiredMissing = requiredMissing.replace(/,\s*$/, ""); //Remove last comma and space
        var replacement = ' and';  //Replace last comma to 'and'
        requiredMissing = requiredMissing.replace(/,([^,]*)$/,replacement+'$1');

        if (requiredMissingCount == 1) {
            alertMessages += "Required field missing: " + requiredMissing + " is a required field.\n";
        }
        else if (requiredMissingCount > 1) {
            alertMessages += "Required fields:\n" + requiredMissing + "\n";
        }

        //Check quantity, thickness
        var quantity = getQuantity($,"#quantity");
        var materialThickness = "";
        if ($("#col-thickness").is(":visible")) {
            materialThickness = getThickness($, "#thickness");
        }

        //Display error messages if present
        if (alertMessages != "") {
            alert(alertMessages);
            return;
        }


        $(this).prop('disabled', true);

        //Set values for some fields (#1) that will be submitted to EMS Server
        var materialThicknessMM = "";
        if (materialThickness != "") {
            $("#processing-thickness").val(materialThickness);
            materialThicknessMM = materialThickness;
            if ($("#thickness-inches").is(":checked")) {
                materialThicknessMM = materialThicknessMM*25.4; //Convert inches to MM
                $("#processing-thickness-inches").prop("checked",true);
            }
            else {
                $("#processing-thickness-mm").prop("checked",true);
            }
        }
        $("#pquantity").val(quantity);
        $("#pthickness").val(materialThicknessMM);
        $("#pcountry").val($("#select-country").find(":selected").text());
        $("#pstate").val($("#select-state").find(":selected").text());
        $("#processing-quantity").val(quantity);
        $("#contact-info-modal").modal();
    });

    $("#shipping-select").on('change', function() {
        var selShippingCost = $(this).find(":selected").data("cost");
        var selShippingCostText = "$"+selShippingCost;
        var selShippingID = $(this).find(":selected").data("methodid");
        if (selShippingCost == "0") {
            selShippingCostText = "FREE";
        }
        $("#shipping-value").data("value",selShippingCost).text(selShippingCostText);
        calculateTotalPrice($);

        if ($("#shipping-radio-" + selShippingID).length != 0) {
            $("#shipping-radio-" + selShippingID).prop('checked', true);
        }
    });

    $("#contact-info-modal").on("hide.bs.modal", function(e) {
        $("#btn-get-quote").prop('disabled', false);
        //window.location.href = "/";
    });


    $("#powder-coat-stocked-color").on("change", function() {
        if ($(this).find(":selected").val() != "") {
            $("#powder-coat-texture-block").show();
            if ($(this).find(":selected").val() == "Custom Color") {
                $("#powder-coat-custcolor-block").show();
                $("#powder-coat-texture").hide();
            }
            else {
                $("#powder-coat-custcolor-block").hide();
                $("#powder-coat-texture").show();

                if ($(this).find(":selected").val() == "Chrome") {
                    $("#powder-coat-texture option[value^=Matte]").hide();
                    if ($("#powder-coat-texture").find(":selected").val() == "Matte") {
                        $("#powder-coat-texture").val("Gloss");
                    }
                }
                else {
                    $("#powder-coat-texture option[value^=Matte]").show();
                }
            }
        }
        else {
            $("#powder-coat-texture-block").hide();
        }
    });

    $("#btn-place-order").click(function(){
        var showFinishParams = false;
        var finishIDs = [];
        var finishGroupsArr = [];
        if ($("#finishes").is(":visible")) {
            $("#finishes").find("option:selected").each(function(){
                var groupName = $(this).closest("optgroup").data("group");
                if (finishGroupsArr.indexOf(groupName) == -1) {
                    finishGroupsArr.push(groupName);
                }
                finishIDs.push($(this).data("finishid"));
            });
        }
        else if ($("#finishes-desktop").is(":visible")) {
            $("#finishes-list-desktop").find("div.dd-active-item").each(function(){
                var groupName = $(this).closest(".material-group").data("group");
                if (finishGroupsArr.indexOf(groupName) == -1) {
                    finishGroupsArr.push(groupName);
                }
                finishIDs.push($(this).data("finishid"));
            });
        }

        //Powder Coat
        if (finishGroupsArr.indexOf("finish-powder-coat") != -1) {
            showFinishParams = true;
            $("#finish-params-powder-coat").show();
        }
        else {
            $("#finish-params-powder-coat").hide();
        }

        //Anodize
        if (finishGroupsArr.indexOf("finish-anodizing") != -1) {
            showFinishParams = true;
            $("#finish-params-anodizing").show();
        }
        else {
            $("#finish-params-anodizing").hide();
        }

        //Blasting / buff polishing / brushing
        if (finishGroupsArr.indexOf("finish-mechanical") != -1) {
            showFinishParams = true;
            $("#finish-params-mechanical").show();
            var finishMechanicalHeaderText = "";
            var finishMechanicalFooterText = "";
            if (finishIDs.indexOf(100) != -1) {//Blasting
                finishMechanicalHeaderText += "blasting, ";
                finishMechanicalFooterText += "blasted, ";
            }
            if (finishIDs.indexOf(92) != -1) {//Buff Polishing
                finishMechanicalHeaderText += "polishing, ";
                finishMechanicalFooterText += "polished, ";
            }
            if (finishIDs.indexOf(65) != -1) {//Brushing
                finishMechanicalHeaderText += "brushing, ";
                finishMechanicalFooterText += "brushed, ";
            }

            var replacement = ' and';
            finishMechanicalHeaderText = finishMechanicalHeaderText.replace(/,\s*$/, ""); //Remove last comma and space
            finishMechanicalFooterText = finishMechanicalFooterText.replace(/,\s*$/, "");
            finishMechanicalHeaderText = finishMechanicalHeaderText.replace(/,([^,]*)$/,replacement+'$1'); //Replace last comma to 'and'
            finishMechanicalFooterText = finishMechanicalFooterText.replace(/,([^,]*)$/,replacement+'$1');

            $("#finish-params-mechanical .group-title span").text(finishMechanicalHeaderText);
            $("#finish-params-mechanical .group-footer span").text(finishMechanicalFooterText);
        }
        else {
            $("#finish-params-mechanical").hide();
        }


        if (!showFinishParams) {
            switchToPage3($);
        }
        else {
            $("#finish-params-modal").modal("show");
        }
    });
    $("#btn-finish-params-submit").click(function(){
        var alertMessages = "";
        if ($("#finish-params-powder-coat").is(":visible")) {
            if ($("#powder-coat-stocked-color").val() == "") {
                alertMessages += "Please indicate powder coat color\n";
                ctrlSetAlert($, $("#powder-coat-stocked-color"), true);
            }
            else {
                ctrlSetAlert($, $("#powder-coat-stocked-color"), false);
            }

            if ($("#powder-coat-texture").val() == "") {
                alertMessages += "Please indicate powder coat texture\n";
                ctrlSetAlert($, $("#powder-coat-texture"), true);
            }
            else {
                ctrlSetAlert($, $("#powder-coat-texture"), false);
            }

            if ($("#powder-coat-custcolor").val() == "") {
                alertMessages += "Please enter powder coat custom color\n";
                ctrlSetAlert($, $("#powder-coat-custcolor"), true);
            }
            else {
                if ($("#powder-coat-custcolor").data("color-valid") == true) {
                    ctrlSetAlert($, $("#powder-coat-custcolor"), false);
                }
                else {
                    ctrlSetAlert($, $("#powder-coat-custcolor"), true);
                    alertMessages += "Please enter valid powder coat custom color\n";
                }
            }
        }

        if ($("#finish-params-anodizing").is(":visible")) {
            if ($("#anodizing-color").val() == "") {
                alertMessages += "Please indicate Anodize color\n";
                ctrlSetAlert($, $("#anodizing-color"), true);
            }
            else {
                ctrlSetAlert($, $("#anodizing-color"), false);
            }

            if ($("#anodizing-texture").is(":visible")) {
                if ($("#anodizing-texture").val() == "") {
                    alertMessages += "Please indicate Anodize texture\n";
                    ctrlSetAlert($, $("#anodizing-texture"), true);
                }
                else {
                    ctrlSetAlert($, $("#anodizing-texture"), false);
                }
            }
        }

        if ($("#finish-params-mechanical").is(":visible")) {

        }

        if (alertMessages == "") {
            switchToPage3($);
        }
        else {
            alert(alertMessages);
        }
    });

    $("#powder-coat-custcolor").keyup(function(){
        var powderCoatCustomColor = $(this).val();
        powderCoatCustomColor = powderCoatCustomColor.replace("SKU", "");
        powderCoatCustomColor = powderCoatCustomColor.replace("SK", "");
        var customColorName = validRALColors[powderCoatCustomColor];
        if (customColorName == undefined || customColorName == "") {
            ctrlSetAlert($, $("#powder-coat-custcolor"), true);
            $("#powder-coat-custcolor-name").text("").hide();
            $("#powder-coat-custcolor").data("color-valid", false);
        }
        else {
            ctrlSetAlert($, $("#powder-coat-custcolor"), false);
            $("#powder-coat-custcolor-name").text(customColorName).slideDown();
            $("#powder-coat-custcolor").data("color-valid", true);
        }
    });

    $("#btn-contact-info-submit, #btn-modify-quote").click(function(){
        if ($(this).is("#btn-contact-info-submit")) {
            alertMessages = '';
            if ($("#firstlastname").val() == "") {
                alertMessages += "Please enter your name.\n";
                ctrlSetAlert($, "#contact-info-modal #firstlastname", true);
            }
            else {
                ctrlSetAlert($, "#contact-info-modal #firstlastname", false);
            }

            if (!isValidEmailAddress($("#contact-info-modal #email").val()) || $("#contact-info-modal #email").val() == "") {
                alertMessages += "Please enter a valid email address.\n";
                ctrlSetAlert($, "#contact-info-modal #email", true);
            }
            else {
                ctrlSetAlert($, "#contact-info-modal #email", false);
            }

            if (!$("#contact-info-modal #i-agree-terms").is(":checked")) {
                alertMessages += "Agreement with Terms of Use and Order Policies is required.\n";
            }

            //Display error messages if present
            if (alertMessages != "") {
                alert(alertMessages);
                return;
            }
            $(this).prop("disabled", true);
            $("#contact-info-modal").modal("hide");

            //Set values for the rest fields (#2) that will be submitted to EMS Server
            var userName = $("#contact-info-modal #firstlastname").val();
            var userEmail = $("#contact-info-modal #email").val();
            $("#pusername").val(userName);
            $("#puseremail").val(userEmail);
            $("#pjobnum").val($("#job-num").val());
            $("#psheetmetalbends").val($("#sheet-metal-with-bends").is(":checked"));

            //Submit the form (show 2nd screen)
            $("#processing-select-params").hide();
            $("#processing-please-wait").show();
            $("#total-price-each").html("&nbsp;");
            $("#total-price").html("&nbsp;");

            $("#processing-file-name").text($("#design-filename").text());
            $("#emsform-instant-quote-page1").fadeOut("slow", function(){
                $("#form-background").removeClass("ems-img-background");
                $("#emsform-instant-quote-page2").show();
                history.pushState({screen: "2"}, null);
                makeEqualHeight($);
            });

            if (typeof SetCookie === "function") {
                SetCookie('Name', userName);
                SetCookie('Email', userEmail);
            }
        }
        else {
            alertMessages = '';
            requiredMissing = '';
            requiredMissingCount = 0;

            //Check Material dropdown and set Material name to form
            if (!setFormMaterialParam($)) {
                alert("Error: Cannot get material");
                return;
            }

            //Set Finishes name to form
            if (!setFormFinishesParam($)) {
                alert("Error: Cannot get finishes");
                return;
            }

            var quantity = getQuantity($,"#processing-quantity");
            var materialThickness = "";
            if ($("#processing-thickness-block").is(":visible")) {
                materialThickness = getThickness($, "#processing-thickness");
            }

            if (alertMessages != "") {
                alert(alertMessages);
                return;
            }

            var materialThicknessMM = "";
            if (materialThickness != "") {
                materialThicknessMM = materialThickness;
                if ($("#processing-thickness-inches").is(":checked")) {
                    materialThicknessMM = materialThicknessMM*25.4; //Convert inches to MM
                }
            }
            $("#pquantity").val(quantity);
            $("#pthickness").val(materialThicknessMM);
            $("#ptolerance").val($("#processing-tolerance").find(":selected").data("mm"));

            $("#irfq-processing-progressbar").css("width", "0%").attr("aria-valuenow", 0);
            $("#processing-select-params").fadeOut("slow", function(){
                $("#processing-please-wait").fadeIn("slow");
                $("#call-bestpricing-note").hide();
                $("#total-price-each").html("&nbsp;");
                $("#total-price").html("&nbsp;");
            });

            $("#table-lead-time").slideUp("fast");
            $("#table-lead-time > tbody").html("");

            $(this).prop("disabled", true);
            makeEqualHeight($);
        }

        $("#subtotal-value,#tax-value,#shipping-value").text("");
        $("#btn-new-file-quote,#btn-modify-quote").hide();
        $("#btn-place-order").prop("disabled",true);
        $("#processing-text").text("Uploading file...");
        $("#processing-dimensions").text("calculating...");
        $("#processing-estimate-time").text("calculating...");


        requestLogID = new Date().valueOf();
        $.ajax({
            url: ajax_object.form_instant_quote_functions,
            type: "GET",
            data: {
                action: "getQuoteNumber",   //also requests log will be written inside this function
                type: "Request sent",
                request_log_id: requestLogID,
                filename: $("#design-filename").text(),
                quantity: $("#pquantity").val(),
                material: $("#pmaterial").val(),
                material_id: $("#pmaterialid").val(),
                job_num: $("#pjobnum").val(),
                email: $("#puseremail").val()
            },
            cache: false,
            dataType: "json",
            success: function(jsonResponse) {
                try {
                    if (jsonResponse.status == "success") {
                        var requestStartTime = new Date();
                        var quoteNumber = jsonResponse.message;
                        $("#quote-number").text(quoteNumber);
                        $("#pquotenumber").val(quoteNumber);

						$.ajax({
							url: 'https://emachineshop.com/emsserver',//96.56.225.51:11000 - https://emachineshop.com/emsserver
							type: 'POST',
							// Form data
							data: new FormData($('#form-browse-for-file')[0]),
							cache: false,
							contentType: false,
							processData: false,
							dataType: 'json',
							context: this,
                            timeout: 30*60*1000,//30 minutes
							// Custom XMLHttpRequest
							xhr: function() {
								var myXhr = $.ajaxSettings.xhr();

								myXhr.upload.addEventListener('progress', function(e) {// For handling the progress of the upload
									if (e.lengthComputable) {
										var percentValue = Math.round(100 * e.loaded / e.total);
										$("#irfq-processing-progressbar").css("width", percentValue+"%").attr("aria-valuenow", percentValue);
										$("#processing-text").text("Uploading file... "+percentValue+"%");

										var estimateTime = Math.round(e.total / 1024 / 500); //1 min per 500kb
										if (estimateTime == 0) {
											estimateTime = 1;
										}
										$("#processing-estimate-time").text(estimateTime+" min");
									}
								}, false);
								myXhr.upload.addEventListener("load", function(e) {// Data uploaded, waiting for price
									$("#processing-text").text(calculatingPriceMessages[0]);

									//var _jQuery = $;
									//calculatingPriceInterval = setInterval(function() {
									//    var processingMsg = calculatingPriceMessages[calculatingPriceMessagesIdx];
									//    _jQuery("#processing-text").fadeOut("slow",function(){
									//        _jQuery(this).text(processingMsg).fadeIn("slow");
									//    });
									//    calculatingPriceMessagesIdx++;
									//    if (calculatingPriceMessagesIdx > calculatingPriceMessages.length-1) {
									//        calculatingPriceMessagesIdx = 0;
									//    }
									//}, 20000);
								}, false);
								myXhr.addEventListener("readystatechange", function(e) {
									try {
										var readyState = e.target.readyState;
										var responseText = e.target.responseText;
										var status = e.target.status;
										if (readyState == 4) {
                                            processServerResponse($,responseText,status);
										}
									}
									catch (e2) {
										$("#processing-manual-quote-button").show();
										$("#processing-place-order").hide();
										$("#processing-manual-quote").show();
                                        $("#processing-dimensions").text("Service Unavailable");
										$("#processing-error-heading").text("File Requires Manual Quotation");
										$("#processing-error-reason").text(e.target.responseText);
                                        makeEqualHeight($);
                                        logResponse($,false,requestStartTime,null);//@@@@1
									}
								}, false);
								return myXhr;
							},
							error: function(xhr) {
								if (xhr.status == "0" && xhr.statusText == "error") {
									$("#processing-place-order").hide();
									$("#processing-manual-quote").show();
                                    $("#processing-dimensions").text("Service Unavailable");
									$("#processing-error-heading").text("File Cannot be Quoted");
									$("#processing-error-reason").html("Service Unavailable.<br>Please [<a href='/contact/'>Notify</a>] eMachineShop");
									$("#processing-manual-quote-button").show();
                                    makeEqualHeight($);
                                    logResponse($,false,requestStartTime,null);//@@@@1
								}
							}
						});
					}
                    else if (jsonResponse.status == 'error') {
                        alert(jsonResponse.message);
                    }
                } catch(e) {
                    alert(e.message);
                }
            },
            error: function(xhr) {
                alert(xhr.responseText);
            }
        });
    });

    //Page 3
    $("#btn-next-step-1").click(function(){
        //Check required fields
        alertMessages = '';
        var requiredMissing = '';
        var requiredMissingCount = 0;
        $("#page3-step1 .required").each(function() {
            if ($(this).is(":enabled") && $(this).hasClass("required")) {
                if ($(this).val() == "" || $(this).val() == null) {
                    ctrlSetAlert($, $(this), true);
                    requiredMissing += $(this).data("required") + ", ";
                    requiredMissingCount++;
                }
                else {
                    ctrlSetAlert($, $(this), false);
                }
            }
        });

        requiredMissing = requiredMissing.replace(/,\s*$/, ""); //Remove last comma and space
        var replacement = ' and';  //Replace last comma to 'and'
        requiredMissing = requiredMissing.replace(/,([^,]*)$/,replacement+'$1');
        if (requiredMissingCount == 1) {
            alertMessages += "Required field missing: " + requiredMissing + " is a required field.\n";
        }
        else if (requiredMissingCount > 1) {
            alertMessages += "Required fields:\n" + requiredMissing + "\n";
        }

        //Check if email is valid
        if (!isValidEmailAddress($("#page3-email").val()) || $("#page3-email").val() == "") {
            alertMessages += "Please enter a valid email address.\n";
            ctrlSetAlert($, "#page3-email", true);
        }
        else {
            ctrlSetAlert($, "#page3-email", false);
        }

        //Display error messages if present
        if (alertMessages != "") {
            alert(alertMessages);
            return;
        }
        $(this).prop("disabled", true);

        //Address, City, State (short) ZIP
        var page3Address = $("#page3-street-address").val();
        var page3City = $("#page3-city").val();
        var page3State = $("#page3-state").val();
        var page3Zip = $("#page3-zip").val();
        $("#page3-shipping-address").text(page3Address+", "+page3City+", "+page3State+" "+page3Zip).show();
        $("#btn-edit-shipping-address").show();

        $("#page3-step1-collapse").collapse("hide");
        $("#page3-step1").addClass("panel-processed");

        $("#page3-step2-collapse").collapse("show");
        $("#page3-step2").removeClass("panel-square-disabled panel-processed").addClass("panel-active");
        $("#page3-step2 > .panel-heading > .panel-header-title > a").removeClass("unclickable clickable");

        var page3Name = $("#page3-name").val();
        var page3Phone = $("#page3-phone").val();
        $("#page3-billing-address").html(page3Name+"<br>"+page3Address+"<br>"+page3City+", "+page3State+" "+page3Zip+"<br>"+page3Phone);
    });

    $("#page3-step2 > .panel-heading").click(function(){
        if ($(this).parent().hasClass("panel-processed")) {
            $("#btn-next-step-1").trigger("click");
        }
    });

    //Button (under step 2) to go to next step (3)
    $("#btn-next-step-2").click(function(){
        $("#page3-step2-collapse").collapse("hide");
        $("#page3-step2").addClass("panel-processed");
        $("#page3-step2 > .panel-heading > .panel-header-title > a").addClass("clickable");

        $("#page3-step3-collapse").collapse("show");
        $("#page3-step3").removeClass("panel-square-disabled panel-processed").addClass("panel-active");
        $("#page3-step3 > .panel-heading > .panel-header-title > a").removeClass("unclickable");
    });

    $("#btn-edit-shipping-address,#link-edit-shipping-address").click(function() {
        $("#page3-step1-collapse").collapse("show");
        $("#page3-step1").removeClass("panel-processed");
        $("#page3-step2-collapse").collapse("hide");
        $("#page3-step2").addClass("panel-square-disabled");
        $("#page3-step3-collapse").collapse("hide");
        $("#page3-step3").addClass("panel-square-disabled");
        $("#btn-edit-shipping-address,#page3-shipping-address").hide();
        $("#page3-step2 > .panel-heading > .panel-header-title > a," +
        "#page3-step3 > .panel-heading > .panel-header-title > a").addClass("unclickable");
        $("#btn-next-step-1").prop("disabled", false);
    });

    $("#page3-street-address").blur(function() {
        //POB, P.O. Box, PO BOX
        if ($(this).val().toUpperCase().indexOf("POB") != -1 ||
            $(this).val().toUpperCase().indexOf("P.O. Box") != -1 ||
            $(this).val().toUpperCase().indexOf("PO BOX") != -1) {
            $("#page3-shipping-note").fadeIn("fast");
        }
        else {
            $("#page3-shipping-note").fadeOut("fast");
        }
    });

    $("#page3-upload-additional-file").click(function() {
        $("#additional-file").trigger("click");
    });

    $("#additional-file").on("change", function() {
        var file = this.files[0];
        var fileExt = file.name.split('.').pop().toLowerCase();
        var errorMsg = '';
        if (file.size > 1024 * 1024 * 64) {
            errorMsg = "File too big: " + file.name + " exceeds our maximum allowed file size.";
        }
        else if (file.size < 100) {
            errorMsg = "File too small: " + file.name + " exceeds our minimum allowed file size.";
        }
        else if (file.name.length > 255) {
            errorMsg = "File name should not exceed 255 characters. Please rename file.";
        }
        else if (fileExt != "pdf") {
            errorMsg = "Uploaded file type not accepted. Please upload a PDF file.";
        }

        if (errorMsg != "") {
            showAlertMessage($,$("#page3-upload-additional-alert"),errorMsg);
            $("#additional-file").val("");
            $("#page3-upload-additional-filename").html("");
            return;
        }

        $("#page3-upload-additional-alert").hide();
        $("#page3-upload-additional-filename").html(file.name);
    });



    if (typeof GetCookie === "function") {
        $("#contact-info-modal #firstlastname").val(GetCookie("Name"));
        $("#contact-info-modal #email").val(GetCookie("Email"));
    }

    var emsVisitorLocationTryCount = 0;
    var emsVisitorLocationTimer = setInterval(function() {
        emsVisitorLocationTryCount++;
        //Get visitor location
        var visitorLocationJSON;
        if (typeof GetCookie === "function") {
            var visitorLocation = GetCookie("visitor_location");
            if (visitorLocation != null && visitorLocation != "" && visitorLocation != undefined) {
                try {
                    visitorLocationJSON = JSON.parse(visitorLocation);
                } catch (e) {
                    console.log(e.message);
                }
            }
        }

        if (emsVisitorLocationTryCount >= 5 || visitorLocationJSON != undefined) {
            clearInterval(emsVisitorLocationTimer);
            if (visitorLocationJSON == undefined) {
                $.ajax({
                    url: ajax_object.form_functions,
                    type: 'GET',
                    data: {action: 'emsGetVisitorLocation'},
                    cache: false,
                    dataType: 'json',
                    success: function (jsonResponse) {
                        try {
                            if (jsonResponse.status == 'success') {
                                setSelectedCountry($, jsonResponse);
                                if (typeof SetCookieExp === "function") {
                                    delete jsonResponse["status"];
                                    SetCookieExp("visitor_location", JSON.stringify(jsonResponse), 30);
                                }
                            }
                            else if (jsonResponse.status == 'error' || jsonResponse.status == 'fail') {
                                console.log(jsonResponse.message);
                            }
                        } catch (e) {
                            console.log(e.message);
                        }
                    },
                    error: function (xhr) {
                        console.log(xhr.responseText);
                    }
                });
            }
            else {
                setSelectedCountry($, visitorLocationJSON);
            }
        }
        emsVisitorLocationTryCount++;
    }, 1000);



    $("#job-num-test").click(function(){
        $("#emsform-instant-quote-page1").hide();
        $("#form-background").removeClass("ems-img-background");
        $("#emsform-instant-quote-page3").show();
    });

    $("#job-num-test").bind("contextmenu",function(e){
        //Show 2nd screen
        $("#processing-select-params").hide();
        $("#processing-please-wait").show();
        $("#total-price-each").html("&nbsp;");
        $("#total-price").html("&nbsp;");
        $("#emsform-instant-quote-page1").hide();
        $("#form-background").removeClass("ems-img-background");
        $("#emsform-instant-quote-page2").show();
        history.pushState({screen: "2"}, null);
        makeEqualHeight($);

        var requestStartTime = new Date();
        processServerResponse($,$("#job-num").val(),"200",requestStartTime);
        return false;
    });

    $(".btn-sw-image").click(function(){
        $("#sw-images-block .btn-sw-image").removeClass("active");
        $("#sw-images-block .sw-image").hide();

        $(this).addClass("active");
        var imgToggle = $(this).data("img-toggle");
        if ($(imgToggle).data("processed") == "0") {
            var currentImgFile = $(imgToggle).data("file");
            if (currentImgFile != undefined && currentImgFile != "") {
                var submitData = new FormData;
                submitData.append("prequest_type", "file");
                submitData.append("prfqid", "24586160");
                submitData.append("password", "price");
                submitData.append("pfile", currentImgFile);

                $.ajax({
                    url: "http://96.56.225.51:11000", // - https://emachineshop.com/emsserver
                    type: "POST",
                    data: submitData,
                    contentType: false,
                    processData: false,
                    context: document.body,
                    dataType: "text",
                    timeout: 60 * 1000,
                    success: function (image_data) {
                        $(imgToggle).attr("src", image_data).data("processed", "1").show();
                    },
                    error: function (xhr) {
                        alert("ERROR: " + xhr.responseText);
                        $(imgToggle).hide();
                    }
                });
            }
        }
        else {
            $(imgToggle).show();
        }
    });
});
