window.fbAsyncInit = function() {
    try {
        //SDK loaded, initialize it
        FB.init({
            appId: "2016385041946738",
            xfbml: true,
            version: "v2.9"
        });

        FB.getLoginStatus(function (response) {
            if (response.status === "connected") {
                jQuery("#review-block-facebook > .logged-in").show(); //user is authorized
                jQuery("#review-block-facebook").css("background-color","white");
            }
            else {
                jQuery("#review-block-facebook > .logged-in").hide();
                jQuery("#review-block-facebook").css("background-color","");
            }
        });
    }
    catch(err) {
        console.log(err.message);
    }
};

/*var gapiStartAppReviewus = function() {
    if (gapi != undefined) {
        try {
            gapi.load('auth2', function() {
                gapi.auth2.init({
                    client_id: '405958871319-fke8io5ja9sjvg6n3r20clotpnnujtfq.apps.googleusercontent.com',
                    scope: 'profile'
                }).then(function(){
                    auth2 = gapi.auth2.getAuthInstance();
                    if (auth2.isSignedIn.get()) {
                        jQuery("#review-block-google > .logged-in").show(); //user is authorized
                    }
                    else {
                        jQuery("#review-block-google > .logged-in").hide();
                    }
                });
            });
        }
        catch(err) {
            console.log(err.message);
        }
    }
};*/


jQuery(document).ready(function ($) {
    $("#stars").show();

    //triggered when modal is about to be shown
    $("#modal-contact").on("show.bs.modal", function(e) {
        var grade = $(e.relatedTarget).data("grade");
        $(this).data("grade",grade);
    });
    $("#modal-reviews").on("show.bs.modal", function(e){
        /*if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
            jQuery("#review-block-google > .logged-in").show(); //user is authorized
        }
        else {
            jQuery("#review-block-google > .logged-in").hide();
        }*/
    });
    $("#modal-reviews,#modal-contact").on("show.bs.modal", function(e) {
        $("#modals-wrapper").detach().prependTo("#page-container");
    });

    $(".star").click(function(){
        $(this).addClass("permahover");
        $(this).nextAll().addClass("permahover");
    });
    $("#modal-reviews,#modal-contact").on("hide.bs.modal", function(e) {
        $(".star").removeClass("permahover");
    });

    $("#review-contact-sendmsg").click(function(){
        $(this).prop("disabled", true);
        var alertMessages = "";
        if (!isValidEmailAddress($("#review-contact-email").val())) {
            alertMessages += "Please enter a valid email address.";
            ctrlSetAlert($, "#review-contact-email", true);
        }
        else {
            ctrlSetAlert($, "#review-contact-email", false);
        }

        if ($("#review-contact-name").val() == "") {
            alertMessages += "Required field missing: 'Your name' is a required field.\n";
            ctrlSetAlert($, "#review-contact-name", true);
        }
        else {
            ctrlSetAlert($, "#review-contact-name", false);
        }


        if (alertMessages != "") {
            alert(alertMessages);
            $(this).prop("disabled", false);
        }
        else {
            var grade = $("#modal-contact").data("grade");
            var name = $("#review-contact-name").val();
            var email = $("#review-contact-email").val();

            $("#stars").css("pointer-events", "none");
            $.ajax({
                url: reviewus_ajax_object.reviewus_sendmsg,
                type: "POST",
                data: {
                    action: "emsGetVisitorLocation",
                    'grade': grade,
                    'contact_name': name,
                    'contact_phone': $("#review-contact-phone").val(),
                    'email': email,
                    'message': $("#review-contact-message").val()
                },
                cache: false,
                dataType: "json",
                success: function (jsonResponse) {
                    try {
                        if (jsonResponse.status == "success") {
                            $("#modal-contact").modal("hide");
                            alert(jsonResponse.message);
                            window.location.href = "/";
                        }
                        else if (jsonResponse.status == 'error') {
                            alert(jsonResponse.message);
                        }
                    } catch (e) {
                        alert(e.message);
                    }
                },
                error: function (xhr) {
                    alert(xhr.responseText);
                }
            });



            var crm_tag = "";
            if (grade == 1 || grade == 2) {
                crm_tag = "EMS Review Us 1-2";
            }
            else if (grade == 3) {
                crm_tag = "EMS Review Us 3";
            }

            $.ajax({
                url: reviewus_ajax_object.edc_functions,
                type: "POST",
                data: {
                    action: "emsAddToAC",
                    crm_tag: crm_tag,
                    crm_email: email,
                    crm_name: name
                },
                cache: false,
                dataType: "json",
                success: function (jsonResponse) {
                    try {
                        if (jsonResponse.status == 'error') {
                            console.log(jsonResponse.message);
                        }
                    } catch (e) {
                        console.log(e.message);
                    }
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }
    });
});