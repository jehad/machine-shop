var notifyErrorSubject = "Quote Form";
var groupsMultiSelectAllowed = ["finish-mechanical","finish-other","finish-none","finish-other"];
var groupsMultiSelectNotAllowed = ["finish-electroplate","finish-powder-coat","finish-chemical-film","finish-anodizing"];

function setEmailInputErrState($,errorState)
{
    if (errorState) {
        ctrlSetAlert($,$("#email"),true);
    }
    else {
        ctrlSetAlert($,$("#email"),false);
        $("#email-alert").hide();
    }
}

function IsPositiveInteger(str)
{
    var n = Math.floor(Number(str));
    return String(n) === str && n > 0;
}

function calculateFullwidth($)
{
    try {
        if ($(window).width() > 768) {
            var totalItemsWidth = 0;
            var rowWidth = $("#dynamic-fullwidth").width();

            //Material, Finishes, Quantity
            $("#dynamic-fullwidth > span, #dynamic-fullwidth > input, #dynamic-fullwidth > label, #dynamic-fullwidth > a").
                not(".material-finish-dropdown").each(function(){
                    if ($(this).is(":visible")) {
                        totalItemsWidth += $(this).outerWidth(true);
                    }
                });

            var dropdownsWidth = (rowWidth-totalItemsWidth-32)/2;
            $(".material-finish-dropdown").innerWidth(dropdownsWidth);


            var maxWidth = $("#emsform-quote-wrapper").width();
            var screenWidth = $(document).width();
            var finishesDDPosLeft = $("#finishes-desktop").offset().left;
            var finishesPosLeft = 0;
            var maxWidthMaterial = maxWidth;
            var maxWidthFinishes = maxWidth;
            if (maxWidth > 800) {
                maxWidthMaterial = 800;
                maxWidthFinishes = 700;
            }
            if (finishesDDPosLeft+maxWidthFinishes > screenWidth) {
                finishesPosLeft = screenWidth-finishesDDPosLeft-maxWidthFinishes-100;
            }

            $("#material-list-desktop").css('max-width',maxWidthMaterial+"px").find(".container").css('max-width',maxWidthMaterial+"px");
            $("#finishes-list-desktop").css('left',finishesPosLeft+"px").css('max-width',maxWidthFinishes+"px").
                find(".container").css('max-width',maxWidthFinishes+"px");
        }
        else {
            $(".material-finish-dropdown").css("width", "");
        }
    } catch (e) {
        console.log(e.message);
    }
}

function checkAlerts($)
{
    //If user uploads .STL or .OBJ, but did not select ABS under 3D printing in Material,
    //Reset material selection and show warning under materials: "STL and OBJ files are accepted for 3D Printing only."
    var showStlObjAlert = false;
    if ($("#upload-design-filename").data("files") != undefined) {
        var designFileExt = $("#upload-design-filename").data("files")[0].filename.split('.').pop().toLowerCase();
        if ($("#upload-design-filename").is(":visible") && (designFileExt == 'obj' || designFileExt == 'stl')) {
            if ($("#material").is(":visible") && $("#material").find(":selected").data("stl-obj-alert") != "0") {
                showStlObjAlert = true;
            }
            else if ($("#material-desktop").is(":visible") && $("#material-desktop").data("stl-obj-alert") != "0") {
                showStlObjAlert = true;
            }
        }
    }

    if (showStlObjAlert) {
        $(".stl-obj-alert").show();
        $("#material-desktop-title").text("- -");
        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $("#material").selectpicker("val", "");
    }
    else {
        $(".stl-obj-alert").hide();
    }


    //Show warning alert when acrylic and polycarbonate is selected that it may become opaque during machining
    var opaqueAlertText = "Clear stock materials such as %selectedMaterialName% may become opaque during the machining process.";
    var showOpaqueAlert = false;
    var selectedMaterialName = "";
    if ($("#material").is(":visible") && $("#material").find(":selected").data("opaque-alert") == "1") {
        selectedMaterialName = $("#material").find(":selected").text();
        showOpaqueAlert = true;
    }
    else if ($("#material-desktop").is(":visible") && $("#material-desktop").data("opaque-alert") == "1") {
        selectedMaterialName = $("#material-list-desktop .dd-active-item").text();
        showOpaqueAlert = true;
    }


    if (!showOpaqueAlert || selectedMaterialName == "") {
        $(".opaque-alert").slideUp("fast");
    }
    else {
        $(".opaque-alert span").text(opaqueAlertText.replace("%selectedMaterialName%", selectedMaterialName));
        $(".opaque-alert").slideDown("fast");
    }
}

function checkShipping($)
{
    //Anything for USA (except Alaska and Hawaii) see Ground (Free) with 2nd day and next day options
    //For anything non USA or Alaska and Hawaii, show Economy and Rush options
    var selCountry = $("#country").find(":selected").text();
    var selState = $("#state").find(":selected").text();
    if (selCountry != "U.S.A." || selState == "AK" || selState == "HI") {
        $("#shipping-method .expshipping").show();
        $("#shipping-method .nexpshipping").hide();
        if ($("#shipping-method").find(":selected").hasClass("nexpshipping")) {
            $("#shipping-method").val("Economy");
        }
    }
    else {
        $("#shipping-method .expshipping").hide();
        $("#shipping-method .nexpshipping").show();
        if ($("#shipping-method").find(":selected").hasClass("expshipping")) {
            $("#shipping-method").val("Ground (Free)");
        }
    }
}

function setSelectedCountry($,jsonLocation)
{
    $("#country").selectpicker("val", jsonLocation.countryCode).data("locationCountry",jsonLocation.country+" ("+jsonLocation.countryCode+")");
    $("#state").val(jsonLocation.region).data("locationRegion",jsonLocation.region);

    if ($("#country").selectpicker("val") == "") {
        $("#country").selectpicker("val","US");
        $("#state").val("NJ");
    }
    $("#country").trigger("change");
}

function EnableRequestSubmittedDialog()
{
  var controls = ["confirmation-dialog-close", "next-part"];
  jQuery("#confirmation-dialog-close").html( "&times;" );
  for(var i=0; i<controls.length;i++)
      jQuery("#" + controls[i]).attr( 'disabled', false );
}

function emsAddToAC($,tagAC,email,firstName)
{
    if (tagAC != "") {
        $.ajax({
            url: ajax_object.form_functions,
            type: "POST",
            data: {
                action: "emsAddToAC",
                crm_tag: tagAC,
                crm_email: email,
                crm_name: firstName
            },
            cache: false,
            dataType: "json",
            success: function (jsonResponse) {
                try {
                    if (jsonResponse.status == 'error') {
                        console.log(jsonResponse.message);
                    }
                } catch (e) {
                    console.log(e.message);
                }
            },
            error: function (xhr) {
                console.log(xhr.responseText);
            }
        });
    }
}

jQuery(document).ready(function ($) {
    $("#emsform-quote-wrapper").css("display","block");
    $('[data-toggle="tooltip"]').tooltip();

    $(".panel-collapse").on("shown.bs.collapse", function(){
        var windowScrollTop = $(window).scrollTop();
        var elementTop = $(this).closest(".panel").offset().top-20;
        if (windowScrollTop > elementTop) {
            $('html, body').animate({
                scrollTop: elementTop
            }, 200);
        }
    });

    $(window).resize(function(){
        calculateFullwidth($);
    });

    $("#dimensions").on("change paste keydown", function(e) {
        var key = String.fromCharCode(e.which).toLowerCase();
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 188, 190]) !== -1 || // Allow: backspace, delete, tab, escape, enter
            ((e.keyCode == 45 || e.keyCode == 65 || e.keyCode == 86 || e.keyCode == 88 || e.keyCode == 67) &&
                (e.ctrlKey === true || e.metaKey === true)) ||// Allow: Ctrl+A,Ctrl+C,Ctrl+X,Ctrl+V, Command+A
            (e.keyCode >= 35 && e.keyCode <= 40) || // Allow: home, end, left, right, down, up
            key == 'x' && $(this).val().split("x").length-1 < 2) { //Allow x character
            return;
        }

        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $('#ftype-cad').click(function(){
        $("#image-text-controls-block").hide();
        $("#tooltip-dimensions,#general-controls-spacer,#part-details-comments,#part-details-design-additional-file").show();
        $("#tooltip-dimensions").attr("data-original-title",
        $("#tooltip-dimensions").data("cad-title")+$("#tooltip-dimensions").data("standart-title"));
        $("#browse-for-file").removeAttr("multiple");

        //Dimensions field will populate under "Design file" and "Additional file" (Only for CAD Form)
        if ($("#upload-design-filename").data("files") != undefined && $("#upload-design-filename").data("files").length != 0) {
            $("#dimensions-group").insertAfter("#photos-text-descr").show();
        }
        else {
            $("#dimensions-group").hide();
        }
    });
    $('#ftype-image-text').click(function(){
        $("#dimensions-group").insertAfter("#photos-text-descr");
        $("#tooltip-dimensions").attr("data-original-title", $("#tooltip-dimensions").data("standart-title"));
        $("#browse-for-file").attr("multiple","multiple");

        $("#part-details-comments,#part-details-design-additional-file").hide();
        $("#tooltip-dimensions,#image-text-controls-block,#general-controls-spacer,#dimensions-group").show();
    });
    $("#ftype-ems-cad").click(function(){
        $("#ems-cad-controls").show();
        $("#general-controls").hide();
        if ($("#ems-cad-image").attr("src") == "") {
            $("#ems-cad-image").attr("src",$("#ems-cad-image").data("src"));
        }
    });
    $("#ftype-cad,#ftype-image-text").click(function(){
        $("#general-controls").show();
        $("#ems-cad-controls").hide();
        calculateFullwidth($);
        checkAlerts($);
    });

    $("#material").on("change", function (e) {
        checkAlerts($);
    });



    //Material and Finish multiview dropdown events and functions
    $("#material-list-desktop .dd-item").click(function(){
        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $(this).addClass("dd-active-item");
        var selectedMaterialName = $(this).text();
        var selectedMaterialGroup = $(this).closest("div.material-group").data("group");
        var selectedMaterialGroupTitle = $(this).closest("div.material-group").data("group-title");
        $("#material-desktop-title").text(selectedMaterialGroupTitle+" - "+selectedMaterialName);

        if ($(this).data("stl-obj-alert") == undefined) {
            $("#material-desktop").data("stl-obj-alert", "1");
        }
        else {
            $("#material-desktop").data("stl-obj-alert", $(this).data("stl-obj-alert"));
        }

        if ($(this).data("opaque-alert") == "1") {
            $("#material-desktop").data("opaque-alert", "1");
        }
        else {
            $("#material-desktop").data("opaque-alert", "");
        }
        checkAlerts($);

        var mobileMaterialValue = $("#material optgroup[data-group='"+selectedMaterialGroup+"'] option:contains('"+selectedMaterialName+"')").attr("value");
        $("#material").selectpicker("val", mobileMaterialValue).trigger("change");
    });
    //Avoid finishes list dropdown menu close on click inside
    $(document).on('click', '#finishes-list-desktop', function (e) {
        e.stopPropagation();
    });
    $("#finishes-list-desktop .dd-item").click(function(){
        if ($(this).hasClass("as-manufactured")) {
            $("#finishes-list-desktop .dd-item").removeClass("dd-active-item");
        }
        else {
            $("#finishes-list-desktop .as-manufactured").removeClass("dd-active-item");
        }

        if ($(this).hasClass("specify-in-comments")) {
            $("#finishes-list-desktop .dd-item").removeClass("dd-active-item");
        }
        else {
            $("#finishes-list-desktop .specify-in-comments").removeClass("dd-active-item");
        }

        var itemPreviouslySelected = false;
        if ($(this).hasClass("dd-active-item")) {
            itemPreviouslySelected = true;
        }

        //Only allow one selection per section
        if (groupsMultiSelectAllowed.indexOf($(this).closest(".material-group").data("group")) == -1) {
            for (var i = 0; i < groupsMultiSelectNotAllowed.length; i++) {
                $("#finishes-list-desktop [data-group='" + groupsMultiSelectNotAllowed[i] + "']").find(".dd-item").removeClass("dd-active-item");
            }
        }

        if (itemPreviouslySelected) {
            $(this).removeClass("dd-active-item");
        }
        else {
            $(this).addClass("dd-active-item");
        }


        var selectedFinishes = "";
        var selectedFinishesTitle = "";
        var selectedFinishesCount = 0;
        $("#finishes-list-desktop .dd-active-item").each(function(){
            var selectedFinish = $(this).text();
            var selectedGroupName = $(this).parent(".material-group").data("groupname");
            selectedFinishesTitle += selectedFinish+", ";
            if (selectedGroupName != undefined) {
                selectedFinish = selectedGroupName+" - "+selectedFinish;
            }
            selectedFinishes += selectedFinish+", ";
            selectedFinishesCount++;
        });
        selectedFinishes = selectedFinishes.replace(/,\s*$/, ""); //Remove last comma and space
        selectedFinishesTitle = selectedFinishesTitle.replace(/,\s*$/, ""); //Remove last comma and space
        $("#finishes-list-desktop").data("selected",selectedFinishes);
        if (selectedFinishesCount > 2) {
            $("#finishes-desktop-title").text(selectedFinishesCount+" items selected");
        }
        else if (selectedFinishesCount == 0) {
            $("#finishes-desktop-title").text(" - - ");
        }
        else {
            $("#finishes-desktop-title").text(selectedFinishesTitle);
        }
    });
    $("#material").on("change", function() {
        var selectedMaterialName = $(this).find(":selected").text();
        var selectedMaterialValue = $(this).find(":selected").val();
        var selectedMaterialGroup = $(this).find(":selected").parent("optgroup").data('group');
        $(this).find(":selected").attr("title",selectedMaterialValue);

        $("#material-list-desktop .dd-item").removeClass("dd-active-item");
        $("#material-desktop-title").text(selectedMaterialValue);
        $("#material-list-desktop div[data-group='"+selectedMaterialGroup+"'] div.dd-item:contains('"+selectedMaterialName+"')").addClass("dd-active-item");
    });
    $("#finishes").on("changed.bs.select", function (e, clickedIndex, isSelected, previousValue) {
        if ($($("#finishes option")[clickedIndex]).hasClass("as-manufactured")) {
            $("#finishes").selectpicker("deselectAll").selectpicker("val", ["As Manufactured"]);
        }
        else if ($($("#finishes option")[clickedIndex]).hasClass("specify-in-comments")) {
            $("#finishes").selectpicker("deselectAll").selectpicker("val", ["Other - Specify in Comments"]);
        }
        else {
            var itemsMultiSelectAllowed = [];
            var itemsMultiSelectNotAllowed = [];
            var i;
            for (i = 0; i < groupsMultiSelectNotAllowed.length; i++) {
                $("#finishes optgroup[data-group='"+groupsMultiSelectNotAllowed[i]+"']").children().each(function () {
                    itemsMultiSelectNotAllowed.push(this.value);
                });
            }
            for (i = 0; i < groupsMultiSelectAllowed.length; i++) {
                $("#finishes optgroup[data-group='"+groupsMultiSelectAllowed[i]+"']").children().each(function () {
                    itemsMultiSelectAllowed.push(this.value);
                });
            }

            //Only allow one selection per section
            var selectedValues = $("#finishes").selectpicker("val");
            var clickedValue = $("#finishes option")[clickedIndex].value;
            if (itemsMultiSelectAllowed.indexOf(clickedValue) == -1 && selectedValues != null) {
                for (i = 0; i < itemsMultiSelectNotAllowed.length; i++) {
                    //removeFrom selectedValues each not allowed value
                    var itemIndex = selectedValues.indexOf(itemsMultiSelectNotAllowed[i]);
                    if (itemIndex !== -1) {
                        selectedValues.splice(itemIndex, 1);
                    }
                }
            }

            if (selectedValues != null) {
                selectedValues.push(clickedValue);

                //Remove as manufactured from selected items
                var asManufacturedIndex = selectedValues.indexOf("As Manufactured");
                if (asManufacturedIndex >= 0) {
                    selectedValues.splice(asManufacturedIndex, 1);
                }

                //Deselect previously selected value (if it was selected)
                if (selectedValues.indexOf(clickedValue) >= 0 && !isSelected) {
                    selectedValues.splice(selectedValues.indexOf(clickedValue), 1);
                }
                $("#finishes").val(selectedValues).selectpicker("render");
            }
        }
    });



    $("#comments").attr("placeholder", $("#comments").data("placeholder-def"));
    $("#tooltip-comments").attr("data-original-title", $("#comments").data("placeholder-def"));


    //Check if email address is valid and email confirm match email
    $("#email").focusout(function(){
        if (!isValidEmailAddress($(this).val()) || $(this).val() == "") {
            showAlertMessage($,$("#email-alert"),"Please enter a valid email address.");
            setEmailInputErrState($,true);
        }
        else {
            setEmailInputErrState($,false);
        }
    });

    $("#country").on("change", function (e) {
        var selCountry = $(this).find(":selected").text();
        if (selCountry == "U.S.A.") {
            $("#state").prop("disabled", false);
            $("#state option[value='N/A']").remove();
        }
        else {
            $('#state').prop('disabled', true).append($('<option/>', {
                value: 'N/A',
                text : 'N/A'
            })).val("N/A");
        }
    });
    $("#state,#country").on("change", function (e) {
        checkShipping($);
    });


    $("#quantity").on('keyup', function() {
        var quantityAlert = false;
        var quantityArr = $(this).val().split(",");
        if (quantityArr.length-1 > 2) {
            quantityAlert = true;
        }
        else {
            for (var i = 0; i < quantityArr.length; i++) {
                var quantityTrimmed = trim(quantityArr[i]);
                if ((i != quantityArr.length-1 && quantityTrimmed == "") ||
                    (!IsPositiveInteger(quantityTrimmed) && quantityTrimmed != "")) {
                    quantityAlert = true;
                    break;
                }
            }
        }

        if (quantityAlert) {
            $(".quantity-alert").show();
        }
        else {
            $(".quantity-alert").hide();
        }
    });




    $('#upload-design-file').click(function(){
        $("#browse-for-file").data("sender","design");
        $("#browse-for-file").data("sender-button",'#upload-design-file');
        $("#browse-for-file").data("prev-uploaded",$("#upload-design-filename").data("files"));
        $("#browse-for-file").data("alertid","upload-design-alert");
        $("#browse-for-file").data("filename-handlerid","upload-design-filename");

        $('#browse-for-file').trigger("click");
    });

    $('#upload-additional-file').click(function(){
        $("#browse-for-file").data("sender","additional");
        $("#browse-for-file").data("sender-button",'#upload-additional-file');
        $("#browse-for-file").data("prev-uploaded",$("#upload-additional-filename").data("files"));
        $("#browse-for-file").data("alertid","upload-additional-alert");
        $("#browse-for-file").data("filename-handlerid","upload-additional-filename");

        $('#browse-for-file').trigger("click");
    });

    $('#upload-photos-file').click(function(){
        try {
            if (ga.q) {
                console.log("GA not working.");
                notifyErrorMessage($,"GA not working",notifyErrorSubject);
            }
            else {
                ga('send', 'event', 'upload-file', 'quote-form'); //BM: Debugging events
            }
        } catch(e) {
            console.log(e.message);
            notifyErrorMessage($,e.message,notifyErrorSubject);
        }

        $("#browse-for-file").data("sender","photos");
        $("#browse-for-file").data("sender-button","#upload-photos-file");
        $("#browse-for-file").data("prev-uploaded",$("#upload-photos-filename").data("files"));
        $("#browse-for-file").data("alertid","upload-photos-alert");
        $("#browse-for-file").data("filename-handlerid","upload-photos-filename");

        $('#browse-for-file').trigger("click");
    });


    $('#browse-for-file').on('change', function() {
        var uploadSender = $("#browse-for-file").data("sender");
        var uploadSenderButton = $("#browse-for-file").data("sender-button");
        var uploadPrevFiles = $("#browse-for-file").data("prev-uploaded");
        var uploadAlertID = "#"+$("#browse-for-file").data("alertid");
        var uploadFilenameHandlerID = "#"+$("#browse-for-file").data("filename-handlerid");

        var allowedExt = [];
        if (uploadSender == "design") {
            allowedExt = ["igs", "iges", "stp", "step", "sldprt", "dwg", "dxf", "amf", "cgr", "eprt", "hcg", "hsf", "ifc",
                "prt", "prtdot", "sat", "sldlfp", "vda", "wrl", "xaml", "x_b", "x_t", "3dxml", "stl", "obj"];
        }
        else if (uploadSender == "additional") {
            allowedExt = ["pdf"];
        }
        else if (uploadSender == "photos") {
            allowedExt = ["pdf", "jpg", "jpeg", "png", "gif", "bmp"];
        }

        for (var i = 0; i < this.files.length; ++i) {
            var errorMsg = '';
            var file = this.files[i];
            var regExpExt = /(?:\.([^.]+))?$/;
            var fileExt = regExpExt.exec(file.name)[1];

            if (file.size > 1024 * 1024 * 64) {
                errorMsg = "File too big: " + file.name + " exceeds our maximum allowed file size.";
            }
            else if (file.size < 100) {
                errorMsg = "File too small: " + file.name + " exceeds our minimum allowed file size.";
            }
            else if (file.name.length > 255) {
                errorMsg = "File name should not exceed 255 characters. Please rename file.";
            }
            else if (fileExt.toLowerCase() == "ems") {
                errorMsg = ".ems files must be <a target='_blank' href='/pricing-2/'>quoted via eMachineShop CAD</a>";
            }
            else if (allowedExt.indexOf(fileExt.toLowerCase()) == -1) {
                if (uploadSender == "design") {
                    errorMsg = "Uploaded file type not accepted. Please upload a IGES, STEP, PRT, SLDPRT, DWG, 2D DXF, XT, STL or OBJ file.";
                }
                else if (uploadSender == "additional") {
                    errorMsg = "Uploaded file type not accepted. Please upload a PDF file.";
                }
                else if (uploadSender == "photos") {
                    errorMsg = "Uploaded file type not accepted. Please upload a PDF, JPG, PNG, GIF and BMP files.";
                }
                else {
                    errorMsg = fileExt + " file extension is not allowed to upload.";
                }
            }

            if (errorMsg != "") {
                showAlertMessage($,$(uploadAlertID),errorMsg);
                $("#browse-for-file").val("");
                $(uploadFilenameHandlerID).html("");
                return;
            }
        }

        $(uploadAlertID).hide();
        $("#previously-uploaded").val(JSON.stringify(uploadPrevFiles));
        $("#upload-sender").val(uploadSender);
        $(uploadSenderButton).prop('disabled', true).text("Uploading...");
        $.ajax({
            url: ajax_object.form_quote_upload,
            type: 'POST',
            // Form data
            data: new FormData($('#form-browse-for-file')[0]),
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            context: this,
            // Custom XMLHttpRequest
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // For handling the progress of the upload
                    myXhr.upload.addEventListener('progress', function(e) {
                        if (e.lengthComputable) {
                            var uploadedPercent = Math.round(100 * e.loaded / e.total);
                            $(uploadSenderButton).prop('disabled', true).text("Uploading... "+uploadedPercent+"%");
                        }
                    } , false);
                }
                return myXhr;
            },
            success: function(jsonResponse) {
                try {
                    $("#browse-for-file").val('');
                    if (jsonResponse.status == 'success') {
                        $(uploadSenderButton).prop('disabled', false).text("Upload");

                        var displayFileName = '';
                        if (jsonResponse.uploaded_files.length == 1) {
                            displayFileName = jsonResponse.uploaded_files[0].filename;
                        }
                        else {
                            displayFileName = jsonResponse.uploaded_files.length+" files selected";
                        }
                        $(uploadFilenameHandlerID).html(displayFileName).data("files",jsonResponse.uploaded_files);


                        //Check STL-OBJ Alert (only for design files)
                        if (uploadSender == "design") {
                            checkAlerts($);
                            //Show-Hide dimensions input and move it if needed
                            if (jsonResponse.uploaded_files.length != 0) {
                                $("#dimensions-group").insertAfter("#part-details-design-additional-file").show();
                            }
                            else {
                                $("#dimensions-group").hide();
                            }
                        }
                        else {
                            $(".stl-obj-alert").hide();
                        }


                        //Show alert messages if presented
                        if (jsonResponse.error_messagees.length > 0) {
                            showAlertMessage($, $(uploadAlertID), jsonResponse.error_messagees.join("<br>"));
                        }
                        else {
                            $(uploadAlertID).hide();
                        }
                    }
                    else if (jsonResponse.status == 'error') {
                        $(uploadSenderButton).prop('disabled', false).text("Upload");
                        showAlertMessage($,$(uploadAlertID),jsonResponse.message);
                    }
                } catch(e) {
                    $(uploadSenderButton).prop('disabled', false).text("Upload");
                    showAlertMessage($,$(uploadAlertID),e.message);
                    notifyErrorMessage($,e.message,notifyErrorSubject);
                }
            },
            error: function(xhr) {
                $(uploadSenderButton).prop('disabled', false).text("Upload");
                $("#browse-for-file").val('');
                showAlertMessage($,$(uploadAlertID),xhr.responseText);
                notifyErrorMessage($,xhr.responseText,notifyErrorSubject);
            }
        });
    });

    $('#btn-submit').click(function(){
        var alertMessages = '';
        var formData = $("#emsform-quote-page").find("textarea, select, input[type=text], input[type=number], input[type=email]").serializeArray();

        //Check required fields
        var requiredMissing = '';
        var requiredMissingCount = 0;
        $(".required:visible").each(function() {
            if ($(this).val() == "" || $(this).val() == null) {
                ctrlSetAlert($,"#"+$(this).attr("id"),true);
                requiredMissing += $(this).data("required")+", ";
                requiredMissingCount++;
            }
            else {
                ctrlSetAlert($,"#"+$(this).attr("id"),false);
            }
        });

        //Check Material dropdown
        if ($("#material").is(":visible")) {
            if ($("#material").selectpicker("val") == null) {
                ctrlSetAlert($, $("#material").parent().find("button.dropdown-toggle"), true);
                requiredMissing += "Material, ";
                requiredMissingCount++;
            }
            else {
                ctrlSetAlert($, $("#material").parent().find("button.dropdown-toggle"), false);
            }
        }
        else if ($("#material-desktop").is(":visible")) {
            if ($("#material-desktop-title").text() == '- -') {
                ctrlSetAlert($, $("#material-desktop"), true);
                requiredMissing += "Material, ";
                requiredMissingCount++;
            }
            else {
                ctrlSetAlert($, $("#material-desktop"), false);
            }
        }

        //Check if design file selected
        if ($("#ftype-cad").is(":checked") &&
            ($("#upload-design-filename").data("files") == undefined || $("#upload-design-filename").data("files").length == 0)) {
            $("#label-design-file").css("color","red");
            requiredMissing += "Design File, ";
            requiredMissingCount++;
        }
        else {
            $("#label-design-file").css("color","");
        }

        //Check if photos file selected or written design description is not empty
        if ($("#ftype-image-text").is(":checked") && $("#upload-photos-filename").data("files") == undefined &&
            $("#written-design-description").val() == "") {
            $("#label-photos-file").css("color","red");
            ctrlSetAlert($, $("#written-design-description"), true);
            alertMessages += "Provide Image(s), Written Description, or both.\n";
        }
        else {
            $("#label-photos-file").css("color","");
            ctrlSetAlert($, $("#written-design-description"), false);
        }

        requiredMissing = requiredMissing.replace(/,\s*$/, ""); //Remove last comma and space
        var replacement = ' and';  //Replace last comma to 'and'
        requiredMissing = requiredMissing.replace(/,([^,]*)$/,replacement+'$1');

        if (requiredMissingCount == 1) {
            alertMessages += "Required field missing: " + requiredMissing + " is a required field.\n";
        }
        else if (requiredMissingCount > 1) {
            alertMessages += "Required fields:\n" + requiredMissing + "\n";
        }

        //Length, width, height
        if (($("#ftype-cad").is(":checked") && $("#dimensions-group").is(":visible")) || $("#ftype-image-text").is(":checked")) {
            var dimensions = $("#dimensions").val();
            var dimensionsArr = dimensions.split("x");

            //Check dimensions
            var dimensionsAlert = false;
            for (var i = 0; i < dimensionsArr.length; i++) {
                dimensionsArr[i] = dimensionsArr[i].replace(",", ".");
                if (!IsPositiveNumber($,dimensionsArr[i])) {
                    ctrlSetAlert($,"#dimensions",true);
                    dimensionsAlert = true;
                }
                else {
                    ctrlSetAlert($,"#dimensions",false);
                }
            }
            if (dimensionsAlert) {
                alertMessages += "Dimensions: Please enter a non-zero positive number.\n";
            }

            //Check part size
            var dimL = dimensionsArr[0] * 1.0;
            var dimW = dimensionsArr[1] * 1.0;
            var dimH = dimensionsArr[2] * 1.0;
            var max_dim = dimL;
            if (dimW > max_dim)
                max_dim = dimW;
            if (dimH > max_dim)
                max_dim = dimH;

            var mid_dim = (dimL + dimW + dimH) / 3.0;
            if (($("#dimensions-inches").is(":checked") && (max_dim > 47.5 || mid_dim > 23.5)) ||
                ($("#dimensions-mm").is(":checked") && (max_dim > 1206.5 || mid_dim > 596.9))) {
                alertMessages += "Part size too large.\n";
                dimensionsAlert = true;
            }
            else {
                formData.push(
                    {name: 'length', value: dimL.toString()},
                    {name: 'width', value: dimW.toString()},
                    {name: 'height', value: dimH.toString()}
                );
            }

            if (dimensionsAlert) {
                ctrlSetAlert($,"#dimensions",true);
            }
            else {
                ctrlSetAlert($,"#dimensions",false);
            }
        }

        //Check quantity
        if ($("#ftype-cad").is(":checked") || $("#ftype-image-text").is(":checked")) {
            var quantityAlert = false;
            var quantityArr = $("#quantity").val().split(",");
            if (quantityArr.length-1 > 2) {
                quantityAlert = true;
            }
            else {
                for (var i = 0; i < quantityArr.length; i++) {
                    var quantityTrimmed = trim(quantityArr[i]);
                    if (!IsPositiveInteger(quantityTrimmed)) {
                        quantityAlert = true;
                        break;
                    }
                }
            }

            if (quantityAlert) {
                ctrlSetAlert($,"#quantity",true);
                alertMessages += "Quantity: Please enter a non-zero positive number.\n";
                $(".quantity-alert").show();
            }
            else {
                ctrlSetAlert($,"#quantity",false);
                $(".quantity-alert").hide();
            }
        }


        //If comments box is empty when submitting and one of these options selected, show warning msg
        if ($("#comments").val() == "") {
            var selectedItemsSelected = "";
            if ($("#finishes").is(":visible") && $("#finishes").selectpicker("val") != null) {
                selectedItemsSelected = "#finishes option:selected";
            }
            else {
                selectedItemsSelected = "#finishes-list-desktop div.dd-active-item";
            }

            var commentsErrState = false;
            $.each($(selectedItemsSelected), function () {
                if ($(this).data("comments") != undefined && $(this).data("comments") != "") {
                    var currentAlertMessage = "Please indicate " + $(this).data("comments") + " in comments.";
                    if (alertMessages.indexOf(currentAlertMessage) == -1) {
                        alertMessages += currentAlertMessage+"\n";
                    }
                    commentsErrState = true;
                }
            }).promise().done(function() {
                ctrlSetAlert($,"#comments",commentsErrState);
            });
        }
        else {
            ctrlSetAlert($,"#comments",false);
        }



        if (!$("#i-agree-terms").is(":checked")) {
            alertMessages += "Agreement with Terms of Use and Order Policies is required.\n";
        }

        //Display error messages if present
        if (alertMessages != "") {
            alert(alertMessages);
            return;
        }


        $(this).prop('disabled', true);

        //Add finishes (mobile and desktop) manually
        if ($("#finishes").is(":visible") && $("#finishes").selectpicker("val") != null) {
            formData.push({name: "finishes", value: $("#finishes").selectpicker("val").join(", ")});
        }
        else if ($("#finishes-desktop").is(":visible") && $("#finishes-list-desktop").data("selected") != undefined &&
            $("#finishes-list-desktop").data("selected") != "") {
            formData.push({name: "finishes", value: $("#finishes-list-desktop").data("selected")});
        }

        //Add material (desktop) manually
        if ($("#material-desktop").is(":visible")) {
            formData.push({name: "material", value: $("#material-desktop-title").text()});
        }

        if ($("#ftype-cad").is(":checked")) {
            formData.push({name: "design-file", value: JSON.stringify($("#upload-design-filename").data("files"))});
            formData.push({name: "additional-file", value: JSON.stringify($("#upload-additional-filename").data("files"))});
        }
        if ($("#ftype-image-text").is(":checked")) {
            formData.push({name: "photos-files", value: JSON.stringify($("#upload-photos-filename").data("files"))});
        }

        if ($("#dimensions-inches").is(":checked")) {
            formData.push({name: "dimensions-units", value: "Inches"});
        }
        else {
            formData.push({name: "dimensions-units", value: "MM"});
        }

        var ga_label=$("#email").val();
        var atpos = ga_label.indexOf( '@' );
        if( atpos > 0 )
           ga_label = ga_label.substr( 0, atpos );


        var ga_status = 'Failed.';
        try { 
            if( ga.q ) //Google analytics not loaded for some reason
                ga_status = 'Google analytics seems blocked.';
            else {
                ga('send', 'event', 'button-click', 'get-quote', ga_label, 1, {transport: 'beacon'}); //BM: Post to google analytics each click to debug
                ga_status = 'Seems success.  label: ' + ga_label + '. transport: beacon';
            }
        } catch(e){
            console.log(e.message);
            notifyErrorMessage($,e.message,notifyErrorSubject);
        }

        formData.push({name: 'google-analytics', value: ga_status});
        formData.push({name: 'page-url', value: window.location.href});

        var locationData = $("#country").data("locationCountry");
        if ($("#state").data("locationRegion") != undefined && $("#state").data("locationRegion") != null) {
            locationData += ", "+$("#state").data("locationRegion");
        }
        formData.push({name: 'location', value: locationData});


        var acTag = "";
        var acEmail = $("#email").val();
        var acName = $("#first-last-name").val();
        if ($("#i-agree-receive-tips").is(":checked")) {
            acTag = "EMS - Newsletter";
        }

        $.ajax({
            url: ajax_object.form_quote_submit,
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            success: function(jsonResponse) {
                try {
                    if (jsonResponse.status == 'success') {
                        if ($("#et-main-area").length != 0) {
                            $("#submit-modal-wrapper").detach().appendTo("#et-main-area");
                        }
                        $("#request-submitted-modal").modal();
                        location.hash = 'submitted';
                    
                        try { 
                            ga('send', 'event', 'quote', 'place', ga_label, 68, {hitCallback: EnableRequestSubmittedDialog, transport: 'beacon'}  ); //BM: Post to google analytics
                        } catch(e){
                            $("#google-analytics-alert").show();
                            notifyErrorMessage($,e.message,notifyErrorSubject);
                        }
                        emsAddToAC($,acTag,acEmail,acName);

                        setTimeout(EnableRequestSubmittedDialog, 1500);         
 
                        if (typeof SetCookie === "function") {
                            SetCookie('Name', acName);
                            SetCookie('Email', acEmail);
                        }
                    }
                    else if (jsonResponse.status == 'error') {
                        $(this).prop('disabled', false);
                        alert(jsonResponse.message);
                        notifyErrorMessage($,jsonResponse.message,notifyErrorSubject);
                    }
                } catch(e) {
                    $(this).prop('disabled', false);
                    alert(e.message);
                    notifyErrorMessage($,e.message,notifyErrorSubject);
                }
            },
            error: function(xhr) {
                $(this).prop('disabled', false);
                emsAddToAC($,acTag,acEmail,acName);
                alert(xhr.responseText);
                notifyErrorMessage($,xhr.responseText,notifyErrorSubject);
            }
        });
    });

    $('#request-submitted-modal').on('hide.bs.modal', function(e) {
        window.location.href = "/";
    });

    $("#next-part").click(function(){
        var currentPageURL = "//" + location.host + location.pathname;
        var newPageURL = currentPageURL + "?cemail=" + $("#email").val() + "&cname=" + $("#first-last-name").val();
        window.location.href = newPageURL;
    });


    try {
        if (typeof GetCookie === "function") {
            if ($("#first-last-name").val() == "") {
                $("#first-last-name").val(GetCookie("Name"));
            }
            if ($("#email").val() == "") {
                $("#email").val(GetCookie("Email"));
            }
        }
    } catch (e) {
        console.log(e.message);
        notifyErrorMessage($,e.message,notifyErrorSubject);
    }

    var emsVisitorLocationTryCount = 0;
    var emsVisitorLocationTimer = setInterval(function(){
        emsVisitorLocationTryCount++;
        //Get visitor location
        try {
            var visitorLocationJSON;
            if (typeof GetCookie === "function") {
                var visitorLocation = GetCookie("visitor_location");
                if (visitorLocation != null && visitorLocation != "" && visitorLocation != undefined) {
                    try {
                        visitorLocationJSON = JSON.parse(visitorLocation);
                    } catch (e) {
                        console.log(e.message);
                        notifyErrorMessage($,e.message,notifyErrorSubject);
                    }
                }
            }

            if (emsVisitorLocationTryCount >= 5 || visitorLocationJSON != undefined) {
                clearInterval(emsVisitorLocationTimer);
                if (visitorLocationJSON == undefined) {
                    $.ajax({
                        url: ajax_object.form_functions,
                        type: 'GET',
                        data: {action: 'emsGetVisitorLocation'},
                        cache: false,
                        dataType: 'json',
                        success: function (jsonResponse) {
                            try {
                                if (jsonResponse.status == 'success') {
                                    setSelectedCountry($, jsonResponse);
                                    if (typeof SetCookieExp === "function") {
                                        delete jsonResponse["status"];
                                        SetCookieExp("visitor_location", JSON.stringify(jsonResponse), 30);
                                    }
                                }
                                else if (jsonResponse.status == 'error' || jsonResponse.status == 'fail') {
                                    console.log(jsonResponse.message);
                                }
                            } catch (e) {
                                console.log(e.message);
                                notifyErrorMessage($,e.message,notifyErrorSubject);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr.responseText);
                        }
                    });
                }
                else {
                    setSelectedCountry($, visitorLocationJSON);
                }
            }
        } catch (e) {
            console.log(e.message);
            clearInterval(emsVisitorLocationTimer);
            notifyErrorMessage($,e.message,notifyErrorSubject);
        }
        emsVisitorLocationTryCount++;
    }, 1000);
});