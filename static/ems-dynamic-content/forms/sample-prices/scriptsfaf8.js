var samplePriceItemsArr;
var currentPage;

function getSamplePriceByID(itemID)
{
    for (var i = 0; i < samplePriceItemsArr.length; i++) {
        if (samplePriceItemsArr[i].id == itemID) {
            return i;
        }
    }
    return -1;
}

function getNextSamplePrice(currentPos)
{
    var i;
    for (i = currentPos+1; i < samplePriceItemsArr.length; i++) {
        if (samplePriceItemsArr[i].show == 1) {
            return i;
        }
    }
    for (i = 0; i < samplePriceItemsArr.length; i++) {
        if (samplePriceItemsArr[i].show == 1) {
            return i;
        }
    }

    return -1;
}
function getPrevSamplePrice(currentPos)
{
    var i;
    for (i = currentPos-1; i >= 0; i--) {
        if (samplePriceItemsArr[i].show == 1) {
            return i;
        }
    }
    for (i = samplePriceItemsArr.length-1; i >= 0; i--) {
        if (samplePriceItemsArr[i].show == 1) {
            return i;
        }
    }
    return -1;
}

function updatePrevNextHeight($)
{
    var arrowsHeight = $("#lightbox-content").outerHeight() - $("#lightbox-modal .modal-header").outerHeight();
    $("#lightbox-prev, #lightbox-next").css("height",arrowsHeight+"px").show();
}

function lighboxFillData($,arrayPos)
{
    $("#lightbox-image").attr("src", samplePriceItemsArr[arrayPos].imageSRC);
    $("#lightbox-title").text(samplePriceItemsArr[arrayPos].name);
    $("#lightbox-material").text(samplePriceItemsArr[arrayPos].material);
    $("#lightbox-finish").text(samplePriceItemsArr[arrayPos].finish);
    //$("#lightbox-process").text(samplePriceItemsArr[arrayPos].process);
    $("#lightbox-size").text(samplePriceItemsArr[arrayPos].size);
    $("#lightbox-type").text(samplePriceItemsArr[arrayPos].type);
    $("#lightbox-cost").text(samplePriceItemsArr[arrayPos].cost);
    $("#lightbox-cost10").text(samplePriceItemsArr[arrayPos].cost10);
    $("#lightbox-cost100").text(samplePriceItemsArr[arrayPos].cost100);
    $("#lightbox-cost1000").text(samplePriceItemsArr[arrayPos].cost1000);
}

function itemsUpdated($)
{
    $(".img-sample-price").click(function(){
        var itemID = $(this).closest(".sample-price-item").data("item-id");
        var arrayPos = getSamplePriceByID(itemID);
        lighboxFillData($,arrayPos);

        //@@@1
        var prevPos = getPrevSamplePrice(arrayPos);
        var nextPos = getNextSamplePrice(arrayPos);
        if (prevPos == -1) {
            $("#lightbox-prev").addClass("prev-next-disabled").data("prev-pos","");
        }
        else {
            $("#lightbox-prev").removeClass("prev-next-disabled").data("prev-pos",prevPos);
        }

        if (nextPos == -1) {
            $("#lightbox-next").addClass("prev-next-disabled").data("next-pos","");
        }
        else {
            $("#lightbox-next").removeClass("prev-next-disabled").data("next-pos",nextPos);
        }

        $("#lightbox-prev, #lightbox-next").hide();
        $("#lightbox-modal-wrapper").detach().appendTo("#et-main-area");
        $("#lightbox-modal").data("array-pos",arrayPos).modal("show");
    });
}

function arrayNotContains(allowedElements,givenElements)
{
    for (var i = 0; i < givenElements.length; i++) {
        if (allowedElements.indexOf(givenElements[i]) != -1) {
            return false;
        }
    }
    return true;
}

function applyPagination($)
{
    var itemsOnPage = parseInt($("#sample-prices-show").val());
    var firstVisibleItem = (currentPage - 1) * itemsOnPage;
    var lastVisibleItem = firstVisibleItem + itemsOnPage;
    var visibleItemsCount = 0;
    for (var i = 0; i < samplePriceItemsArr.length; i++) {
        if (samplePriceItemsArr[i].show == 1) {
            if (visibleItemsCount < firstVisibleItem || visibleItemsCount >= lastVisibleItem) {
                samplePriceItemsArr[i].show = 0;
            }
            visibleItemsCount++;
        }
    }
    $("#sample-prices-pagination").pagination("updateItems", visibleItemsCount);
}

function checkItemsVisibility($)
{
    var allowedMaterials = [];
    //var allowedProcesses = [];
    var allowedFinishes = [];
    var allowedTypes = [];

    var allSelected = $("#filter-groups > .group-material").find(".btn-opt-all").hasClass("btn-primary");
    $("#filter-groups > .group-material").find(".btn-opt").each(function(){
        if (allSelected || $(this).hasClass("btn-primary")) {
            allowedMaterials.push($(this).text());
        }
    });

    /*allSelected = $("#filter-groups > .group-process").find(".btn-opt-all").hasClass("btn-primary");
    $("#filter-groups > .group-process").find(".btn-opt").each(function(){
        if (allSelected || $(this).hasClass("btn-primary")) {
            allowedProcesses.push($(this).text());
        }
    });*/

    allSelected = $("#filter-groups > .group-finish").find(".btn-opt-all").hasClass("btn-primary");
    $("#filter-groups > .group-finish").find(".btn-opt").each(function(){
        if (allSelected || $(this).hasClass("btn-primary")) {
            allowedFinishes.push($(this).text());
        }
    });

    allSelected = $("#filter-groups > .group-type").find(".btn-opt-all").hasClass("btn-primary");
    $("#filter-groups > .group-type").find(".btn-opt").each(function(){
        if (allSelected || $(this).hasClass("btn-primary")) {
            allowedTypes.push($(this).text());
        }
    });


    for (var i = 0; i < samplePriceItemsArr.length; i++) {
        var samplePrice = samplePriceItemsArr[i];
        if (allowedMaterials.indexOf(samplePrice.generalMaterial) == -1 ||
            /*allowedProcesses.indexOf(samplePrice.process) == -1 ||*/
            arrayNotContains(allowedFinishes,samplePrice.finish) ||
            arrayNotContains(allowedTypes,samplePrice.type)) {
            samplePriceItemsArr[i].show = 0;
        }
        else {
            samplePriceItemsArr[i].show = 1;
        }
    }
    applyPagination($);


    var itemNum = 0;
    var visibleItemsCount = 0;
    var itemHTMLTemplate = $("#sample-price-items-container").data("item-html");
    $("#sample-price-items-container").find(".sample-price-item").each(function() {
        while (itemNum < samplePriceItemsArr.length && samplePriceItemsArr[itemNum].show != 1) {
            itemNum++;
        }

        if (itemNum >= samplePriceItemsArr.length) {
            $(this).data("item-id","").closest(".item-container").data("show","0").fadeOut("fast");
        }
        else {
            var itemHTML = itemHTMLTemplate.replace("%image_src%", samplePriceItemsArr[itemNum].imageSRC);
            itemHTML = itemHTML.replace("%name%", samplePriceItemsArr[itemNum].name);
            itemHTML = itemHTML.replace("%size%", samplePriceItemsArr[itemNum].size);
            itemHTML = itemHTML.replace("%material%", samplePriceItemsArr[itemNum].material);
            //itemHTML = itemHTML.replace("%process%", samplePriceItemsArr[itemNum].process);
            itemHTML = itemHTML.replace("%finish%", samplePriceItemsArr[itemNum].finish.join(", "));
            itemHTML = itemHTML.replace("%type%", samplePriceItemsArr[itemNum].type.join(", "));
            itemHTML = itemHTML.replace("%cost%", samplePriceItemsArr[itemNum].cost);
            itemHTML = itemHTML.replace("%cost10%", samplePriceItemsArr[itemNum].cost10);
            itemHTML = itemHTML.replace("%cost100%", samplePriceItemsArr[itemNum].cost100);
            itemHTML = itemHTML.replace("%cost1000%", samplePriceItemsArr[itemNum].cost1000);

            $(this).data("item-id",samplePriceItemsArr[itemNum].id).html(itemHTML).closest(".item-container").fadeIn("fast");
            itemNum++;
            visibleItemsCount++;
        }
    }).promise().done(function(){
        itemsUpdated($);
    });


    if (visibleItemsCount == 0) {
        $("#emsform-sample-prices button.request-quote:last-child").last().hide();
    }
    else {
        $("#emsform-sample-prices button.request-quote:last-child").last().show();
    }
}

jQuery(document).ready(function ($) {
    $(".btn-opt").click(function(e){
        if ($(this).hasClass("btn-primary") && $(this).closest(".btn-group").find(".btn-primary").length == 1) {
            e.preventDefault();
            return false;
        }

        if ($(this).hasClass("btn-primary")) {
            $(this).removeClass("btn-primary").addClass("btn-default");
        }
        else {
            $(this).addClass("btn-primary").removeClass("btn-default");
        }

        var groupSelectedAll = true;
        $(this).closest(".btn-group").find(".btn-opt").each(function(){
            if (!$(this).hasClass("btn-primary")) {
                groupSelectedAll = false;
                return false;
            }
        });

        if (groupSelectedAll) {
            $(this).closest(".btn-group").find(".btn-opt").removeClass("btn-primary").addClass("btn-default");
            $(this).closest(".btn-group").find(".btn-opt-all").removeClass("btn-default").addClass("btn-primary");
            this.blur();
        }
        else {
            $(this).closest(".btn-group").find(".btn-opt-all").removeClass("btn-primary").addClass("btn-default");
        }

        currentPage = 1;
        checkItemsVisibility($);
        $("#sample-prices-pagination").pagination("drawPage", 1);
    });

    $(".btn-opt-all").click(function(){
        if (!$(this).hasClass("btn-primary")) {
            $(this).closest(".btn-group").find(".btn").removeClass("btn-primary").addClass("btn-default");
            $(this).addClass("btn-primary");
            currentPage = 1;
            checkItemsVisibility($);
            $("#sample-prices-pagination").pagination("drawPage", 1);
        }
    });

    $("#filter").on("change", function (e) {
        var group = $(this).find(":selected").data("group");
        if (group == "") {
            $("#filter-groups > .btn-group").addClass("spacer5").slideDown("fast");
            $("#filter-groups > .btn-group:first").removeClass("spacer5");
            $("#filter-groups > .btn-group").find("spacer5");
        }
        else {
            $("#filter-groups > .btn-group").removeClass("spacer5").hide();
            $("#filter-groups > ."+group).show();
        }

        currentPage = 1;
        $("#filter-groups .btn-opt").removeClass("btn-primary").addClass("btn-default");
        $("#filter-groups .btn-opt-all").removeClass("btn-default").addClass("btn-primary");
        checkItemsVisibility($);
        $("#sample-prices-pagination").pagination("drawPage", 1);
    });


    $("#lightbox-prev").click(function(){
        if ($(this).hasClass("prev-next-disabled")) {
            return;
        }

        var nextPos = $("#lightbox-modal").data("array-pos");
        var currentPos = $("#lightbox-prev").data("prev-pos");
        var prevPos = getPrevSamplePrice(currentPos);
        if (prevPos == -1) {
            $("#lightbox-prev").addClass("prev-next-disabled").data("prev-pos","");
        }
        else {
            $("#lightbox-prev").removeClass("prev-next-disabled").data("prev-pos",prevPos);
        }
        $("#lightbox-next").data("next-pos",nextPos).removeClass("prev-next-disabled");
        lighboxFillData($,currentPos);
        updatePrevNextHeight($);
        $("#lightbox-modal").data("array-pos",currentPos);
    });
    $("#lightbox-next").click(function(){
        if ($(this).hasClass("prev-next-disabled")) {
            return;
        }

        var prevPos = $("#lightbox-modal").data("array-pos");
        var currentPos = $("#lightbox-next").data("next-pos");
        var nextPos = getNextSamplePrice(currentPos);
        if (nextPos == -1) {
            $("#lightbox-next").addClass("prev-next-disabled").data("next-pos","");
        }
        else {
            $("#lightbox-next").removeClass("prev-next-disabled").data("next-pos",nextPos);
        }
        $("#lightbox-prev").data("prev-pos",prevPos).removeClass("prev-next-disabled");
        lighboxFillData($,currentPos);
        updatePrevNextHeight($);
        $("#lightbox-modal").data("array-pos",currentPos);
    });

    $("#lightbox-modal").on('shown.bs.modal', function () {
        updatePrevNextHeight($);
    });

    $("#lightbox-image").load(function(){
        updatePrevNextHeight($);
    });

    var samplePriceItemsJSON = $("#sample-price-items-container").data("items-json");
    $("#sample-price-items-container").data("items-json","");
    samplePriceItemsArr = $.map(samplePriceItemsJSON, function(value, index) {
        return [value];
    });
    itemsUpdated($);


    //Initialize pagination
    currentPage = 1;
    $("#sample-prices-pagination").pagination({
        items: samplePriceItemsArr.length,
        itemsOnPage: $("#sample-prices-show").val(),
        prevText: "«",
        nextText: "»",
        onPageClick: function(pageNumber, event) {
            currentPage = pageNumber;
            checkItemsVisibility($);
        }
    });
    $("#sample-prices-show").on("change", function (e) {
        if ($(this).val() == "All") {
            $("#sample-prices-pagination").slideUp("fast");
        }
        else {
            $("#sample-prices-pagination").pagination("updateItemsOnPage", $(this).val()).slideDown("fast");
        }
        checkItemsVisibility($);
    });
    checkItemsVisibility($);

    $("#sample-price-items-container,#request-quote-btn2").slideDown("fast");
});