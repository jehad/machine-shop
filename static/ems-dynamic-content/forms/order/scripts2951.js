var creditCardType;
var notifyErrorSubject = "Order Form";

function confirmExit() {
    return "You have attempted to leave this page. Please wait until form will process the order. Your changes will be lost if you will leave this page. Are you sure you want to exit this page?";
}

function setEmailInputErrState($,errorState)
{
    if (errorState) {
        ctrlSetAlert($,"#email",true);
    }
    else {
        ctrlSetAlert($,"#email",false);
        $("#email-alert").hide();
    }
}

function uploadThreadingFileEvent($,element)
{
    var blockNum = $(element).closest(".quote-details-item").data("quote-block-num");
    $("#browse-for-file").data("block-num",blockNum).trigger("click");
}

function deleteQuoteBlockEvent($,element)
{
    var $quoteDetailsItem = $(element).closest(".quote-details-item");

    if ($($quoteDetailsItem).find("#add-another-quote").length != 0) {
        $($quoteDetailsItem).find("#add-another-quote").detach().prependTo($("#quote-details-block").find(".add-another-quote-block").eq(-2));
    }

    $($quoteDetailsItem).closest(".quote-details-item").slideUp("fast", function(){
        $($quoteDetailsItem).remove();
        checkQuoteBlockItems($);
    });
}

function checkQuoteBlockItems($)
{
    if ($(".quote-details-item").length == 1) {
        $("button.delete-quote-block").animate({width: "toggle"},"fast");
    }
    else {
        $("button.delete-quote-block").show();
    }
}

function setSelectedCountry($,jsonLocation)
{
    $("#country,#shipping-country").selectpicker("val", jsonLocation.countryCode);
    $("#state,#shipping-state").val(jsonLocation.region);
    if ($("#country").selectpicker("val") == "") {
        $("#country,#shipping-country").selectpicker("val","US");
        $("#state,#shipping-state").val("NJ");
    }
    $("#country,#shipping-country").trigger("change");
}

jQuery(document).ready(function ($) {
    $("#emsform-order-wrapper").css("display","block");
    $('[data-toggle="tooltip"]').tooltip();
    $("#phone").mask("(999) 999-9999? x99999",{autoclear: false});
    $("#cvv").mask("9999",{autoclear: false, placeholder:" "});

    var creditCardsArr = ["amex", "discover", "mastercard", "visa"];
    var cleaveCCNumber = new Cleave('#card-number', {
        creditCard: true,
        onCreditCardTypeChanged: function (type) {
            creditCardType = type;
            $("#img-cc-block > .img-cc:not(.img-cc-active)").addClass("img-cc-active");
            if (creditCardsArr.indexOf(type) >= 0) {
                $("#img-cc-block > .img-cc-active").removeClass("img-cc-active");
                $("#img-cc-"+type).addClass("img-cc-active");
            }
        }
    });
    var cleaveCCMonthYear = new Cleave('#exp-month-year', {date: true, datePattern: ['m', 'y']});

    $(".quoted-price-input").focusin(function(){
        $(this).css("padding-left","20px");
        $(this).closest(".quoted-price-pref").addClass("has-sign");
    }).focusout(function(){
        if ($(this).val() == "") {
            $(this).css("padding-left", "");
            $(this).closest(".quoted-price-pref").removeClass("has-sign");
        }
    });

    $(".panel-collapse").on("shown.bs.collapse", function(e){
        if ($(e.target).is($("#shipping-address-block"))) {
            return;
        }

        var windowScrollTop = $(window).scrollTop();
        var elementTop = $(this).closest(".panel").offset().top-20;
        if (windowScrollTop > elementTop) {
            $('html, body').animate({
                scrollTop: elementTop
            }, 200);
        }
    });

    //Check if email address is valid
    $("#email").focusout(function(){
        var emailValid = isValidEmailAddress($("#email").val()) || $("#email").val() == "";
        if (!emailValid) {
            showAlertMessage($,$("#email-alert"),"Please enter a valid email address.");
            setEmailInputErrState($,true);
        }
        else if (emailValid) {
            setEmailInputErrState($,false);
        }
    });


    $("#country,#shipping-country").on("change", function (e) {
        var selCountry = $(this).find(":selected").text();
        var stateElement = $(this).data("stateid");
        var provinceElement = $(this).data("provinceid");
        if (selCountry == "U.S.A.") {
            $(stateElement).show();
            $(stateElement).prop("disabled", false);
            $(provinceElement).hide();
            $(provinceElement).prop("disabled", true);
        }
        else {
            $(stateElement).hide();
            $(stateElement).prop("disabled", true);
            $(provinceElement).show();
            $(provinceElement).prop("disabled", false);
        }
    }).trigger("change");


    checkQuoteBlockItems($);
    $(".upload-threading-file").click(function(){
        uploadThreadingFileEvent($,$(this));
    });
    $(".delete-quote-block").off("click").click(function(){
        deleteQuoteBlockEvent($,$(this));
    });

    $("#add-another-quote").click(function(){
        var quoteDetailsNum = $(".quote-details-item:last").data("quote-block-num")+1;

        //Clone, set new ID and data
        $(".quote-details-item:last").clone().hide().appendTo("#quote-details-block").slideDown().
            attr("id","quote-details-block-"+quoteDetailsNum).data("quote-block-num",quoteDetailsNum);
        $("#quote-details-block").find(".add-another-quote-block:last #add-another-quote").detach();
        $(this).detach().prependTo($("#quote-details-block").find(".add-another-quote-block:last"));

        //Clear inputs in clone, hide alert
        $("#quote-details-block").find(".quote-details-item:last input").val("");
        $("#quote-details-block").find(".quote-details-item:last .upload-threading-alert").hide();
        $("#quote-details-block").find(".quote-details-item:last .upload-threading-filename").html("").data("tempname","").data("filename","");
        $("#quote-details-block").find(".quote-details-item:last .quoted-price-input").css("padding-left", "").closest(".quoted-price-pref").removeClass("has-sign");
        checkQuoteBlockItems($);

        //Remove old events and assign new
        $('[data-toggle="tooltip"]').tooltip();
        $(".upload-threading-file").off("click").click(function(){
            uploadThreadingFileEvent($,$(this));
        });
        $(".delete-quote-block").off("click").click(function(){
            deleteQuoteBlockEvent($,$(this));
        });

        $(".quoted-price-input").off('focusin').off('focusout').focusin(function(){
            $(this).css("padding-left","20px");
            $(this).closest(".quoted-price-pref").addClass("has-sign");
        }).focusout(function(){
            if ($(this).val() == "") {
                $(this).css("padding-left", "");
                $(this).closest(".quoted-price-pref").removeClass("has-sign");
            }
        });
    });
    
    $("#shipping-address-different").click(function(){
        var changeStateElements = "#shipping-name,#shipping-address,#shipping-country,#shipping-state,#shipping-province,#shipping-city,#shipping-zip-code";
        if ($(this).is(":checked")) {
            $(changeStateElements).addClass("required");
            $(changeStateElements).prop("disabled", false);
            $("#shipping-address-block").slideDown("slow");
        }
        else {
            $(changeStateElements).removeClass("required");
            $(changeStateElements).prop("disabled", true);
            $("#shipping-address-block").slideUp("slow");
        }
        $("#shipping-country").selectpicker('refresh').trigger("change");
    });


    $("#browse-for-file").on("change", function() {
        var blockNum = $(this).data("block-num");
        var $uploadThreadingAlert = $("#quote-details-block-"+blockNum).find(".upload-threading-alert");

        var errorMsg = '';
        var file = this.files[0];

        var regExpExt = /(?:\.([^.]+))?$/;
        var fileExt = regExpExt.exec(file.name)[1];
        var allowedExt = ["pdf", "ems"];

        if (file.size > 1024*1024*10) {
            errorMsg = "File too big: "+file.name+" exceeds our maximum allowed file size.";
        }
        else if (file.size < 1024) {
            errorMsg = "File too small: "+file.name+" exceeds our minimum allowed file size.";
        }
        else if (file.name.length > 255) {
            errorMsg = "File name should not exceed 255 characters. Please rename file.";
        }
        else if (allowedExt.indexOf(fileExt.toLowerCase()) == -1) {
            errorMsg = fileExt+' file extension is not allowed to upload';
        }

        if (errorMsg != '') {
            showAlertMessage($,$uploadThreadingAlert,errorMsg);
            $("#browse-for-file").val("");
            return;
        }
        else {
            $($uploadThreadingAlert).hide();
        }

        $.ajax({
            url: ajax_object.form_order_upload,
            type: 'POST',
            // Form data
            data: new FormData($('#form-browse-for-file')[0]),
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            context: this,
            // Custom XMLHttpRequest
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    // For handling the progress of the upload
                    myXhr.upload.addEventListener('progress', function(e) {
                        if (e.lengthComputable) {
                            $('progress').attr({
                                value: e.loaded,
                                max: e.total
                            });
                        }
                    } , false);
                }
                return myXhr;
            },
            success: function(jsonResponse) {
                try {
                    $("#browse-for-file").val('');
                    if (jsonResponse.status == 'success') {
                        $($uploadThreadingAlert).hide();
                        $("#quote-details-block-"+blockNum).find(".upload-threading-filename").html(jsonResponse.filename)
                            .data("tempname",jsonResponse.tempfile)
                            .data("filename",jsonResponse.filename);
                    }
                    else if (jsonResponse.status == 'error') {
                        showAlertMessage($,$uploadThreadingAlert,jsonResponse.message);
                    }
                } catch(e) {
                    showAlertMessage($,$uploadThreadingAlert,e.message);
                    notifyErrorMessage($,e.message,notifyErrorSubject);
                }
            },
            error: function(xhr) {
                $("#browse-for-file").val('');
                showAlertMessage($,$uploadThreadingAlert,xhr.responseText);
                notifyErrorMessage($,xhr.responseText,notifyErrorSubject);
            }
        });
    });


    $('#btn-submit').click(function() {
        var alertMessages = '';

        var emailValid = isValidEmailAddress($("#email").val()) || $("#email").val() == "";
        if (!emailValid) {
            alertMessages = "Please enter a valid email address.\n";
        }

        //Check required fields
        var requiredMissing = '';
        var requiredMissingCount = 0;
        $(".required,#state,#province,#shipping-state,#shipping-province,#phone-number").each(function () {
            if ($(this).is(":enabled") && $(this).hasClass("required")) {
                if ($(this).val() == "" || $(this).val() == null) {
                    ctrlSetAlert($, $(this), true);
                    requiredMissing += $(this).data("required") + ", ";
                    requiredMissingCount++;
                }
                else {
                    ctrlSetAlert($, $(this), false);
                }
            }
        });


        //Check required fields
        requiredMissing = requiredMissing.replace(/,\s*$/, ""); //Remove last comma and space
        var replacement = ' and';  //Replace last comma to 'and'
        requiredMissing = requiredMissing.replace(/,([^,]*)$/, replacement + '$1');
        if (requiredMissingCount == 1) {
            alertMessages += "Required field missing: " + requiredMissing + " is a required field.\n";
        }
        else if (requiredMissingCount > 1) {
            alertMessages += "Required fields:\n" + requiredMissing + "\n";
        }

        //Check if quote number is valid
        var notValidQuoteNumber = false;
        $("#quote-details-block input[name='quote-number']").each(function () {
            var orderIDErrorMsg = CheckEMSOrderID($(this).val(), "Quote number is not correct. Please enter valid quote number.");
            if (orderIDErrorMsg != "") {
                if (!notValidQuoteNumber) {
                    notValidQuoteNumber = true;
                    alertMessages += orderIDErrorMsg + "\n";
                }
                ctrlSetAlert($, $(this), true);
            }
            else {
                ctrlSetAlert($, $(this), false);
            }
        });

        //Check Credit Card information
        var ccExpMonthYear = $("#exp-month-year").val();
        var ccExpMonthYearArr = ccExpMonthYear.split("/");
        ccExpMonthYearArr[1] = "20" + ccExpMonthYearArr[1];
        if (typeof CheckCreditCard === "function") {
            var creditCardError = CheckCreditCard("", $("#card-number").val(), ccExpMonthYearArr[0], ccExpMonthYearArr[1], $("#cvv").val());
            if (creditCardError != "") {
                alertMessages += creditCardError + "\n";
                ctrlSetAlert($, "#card-number, #exp-month-year, #cvv", true);
            }
            else {
                ctrlSetAlert($, "#card-number, #exp-month-year, #cvv", false);
            }
        }

        //Check I agree ... checkbox
        if (!$("#i-agree-terms").is(":checked")) {
            alertMessages += "You must agree to the Terms of Use and Order Policies and review the pre-order checklist.\n";
        }

        //Display error messages if present
        if (alertMessages != "") {
            alert(alertMessages);
            return;
        }
        else {
            window.onbeforeunload = confirmExit;
        }


        $(this).prop("disabled", true);
        var formData = $("#emsform-inputs-submit").find("textarea, select:enabled, input[type=text]:enabled, " +
            "input[type=number]:enabled, input[type=email]:enabled").serializeArray();

        var allowPublishStill = $("#allow-publish-still").is(":checked") ? 'yes' : 'no';
        formData.push({name: "allow-publish-still", value: allowPublishStill});
        formData.push({name: "credit-card-type", value: creditCardType});
        formData.push({name: "cc-exp-month", value: ccExpMonthYearArr[0]});
        formData.push({name: "cc-exp-year", value: ccExpMonthYearArr[1]});


        var quoteDetailsBlocksCount = $("#quote-details-block > .quote-details-item:last").data("quote-block-num");
        var quoteDetailsCount = 0;
        var gaOrderDetailsArr = [];
        for (var i = 1; i <= quoteDetailsBlocksCount; i++) {
            if ($("#quote-details-block-"+i).length != 0) {
                quoteDetailsCount++;
                var quoteNumber = $("#quote-details-block-" + i).find("input[name='quote-number']").val();
                var quotedPrice = $("#quote-details-block-" + i).find("input[name='quoted-price']").val();
                gaOrderDetailsArr.push({"quoteNumber":quoteNumber, "quotedPrice":quotedPrice});

                formData.push({
                    name: "quote-number-" + quoteDetailsCount,
                    value: quoteNumber
                });
                formData.push({
                    name: "quoted-price-" + quoteDetailsCount,
                    value: quotedPrice
                });
                formData.push({
                    name: "threading-file-name-" + quoteDetailsCount,
                    value: $("#quote-details-block-" + i).find(".upload-threading-filename").data("filename")
                });
                formData.push({
                    name: "threading-file-temp-" + quoteDetailsCount,
                    value: $("#quote-details-block-" + i).find(".upload-threading-filename").data("tempname")
                });
            }
        }
        formData.push({name: "quote-details-count", value: quoteDetailsCount});


        $.ajax({
            url: ajax_object.form_order_submit,
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            success: function(jsonResponse) {
                try {
                    if (jsonResponse.status == 'success') {
                        var google_analytics_error = '';
                        if (typeof ga === "function") {
                            for (var i = 0; i < gaOrderDetailsArr.length; i++) {
                                var order_num = parseInt(gaOrderDetailsArr[i].quoteNumber);
                                var order_total = parseFloat(gaOrderDetailsArr[i].quotedPrice);

                                if (!isNaN(order_num) && !isNaN(order_total)) {
                                    var event_value = Math.round(order_total);
                                    ga('require', 'ecommerce');
                                    ga('ecommerce:addTransaction', {'id': "" + order_num,  'affiliation': 'EMS', 'revenue': "" + order_total, 'shipping': '0.0', 'tax': '0.0' });
                                    ga('ecommerce:addItem', { 'id': "" + order_num,'name': 'Custom part', 'sku': '', 'category': 'RFQ', 'price': "" + order_total, 'quantity': '1' });
                                    ga('ecommerce:send');
                                    ga('send', 'event', 'order', 'place', 'RFQ ORDER',  event_value );
                                    if (typeof fbq === "function") {//Facebook pixel tracking
                                        fbq('track', 'Purchase', {currency: 'USD', value: order_total, content_name: 'Custom Part - RFQ', num_items: 1});
                                    }
                                }
                                else {
                                    google_analytics_error = 'order_num or order_total is NAN';
                                }
                            }
                        }
                        else {
                            google_analytics_error = 'ga(...) not defined';
                        }

                        window.onbeforeunload = null;
                        $("#order-submitted-modal").modal();
                        location.hash = 'submitted';
                        $("#next-order").data("params", jsonResponse.nextlink);

                        if (google_analytics_error != '') {
                            var msg = $("#google-analytics-alert").html();
                            msg = msg + ": " + google_analytics_error;
                            $("#google-analytics-alert").html( msg );
                            $("#google-analytics-alert").show();
                            notifyErrorMessage($,msg,notifyErrorSubject);
                        }
         

                        if (typeof SetCookie === "function") {
                            SetCookie('Name', $("#name-on-card").val());
                            SetCookie('Email', $("#email").val());
                        }
                    }
                    else if (jsonResponse.status == 'error') {
                        window.onbeforeunload = null;
                        alert(jsonResponse.message);
                        $("#btn-submit").prop("disabled", false);
                    }
                } catch(e) {
                    window.onbeforeunload = null;
                    alert(e.message);
                    $("#btn-submit").prop("disabled", false);
                    notifyErrorMessage($,e.message,notifyErrorSubject);
                }
            },
            error: function(xhr) {
                window.onbeforeunload = null;
                alert(xhr.responseText);
                $("#btn-submit").prop("disabled", false);
                notifyErrorMessage($,xhr.responseText,notifyErrorSubject);
            }
        });
    });

    $('#order-submitted-modal').on('hide.bs.modal', function(e) {
        window.location.href = "/";
    });

    $("#next-order").click(function(){
        var currentPageURL = "//" + location.host + location.pathname;
        var shippingAddrDifferent = $("#shipping-address-different").is(":checked") ? 1 : 0; //bool to int
        window.location.href = currentPageURL + "?cshdif=" + shippingAddrDifferent + "&params=" + $(this).data("params");
    });


    if (typeof GetCookie === "function") {
        if ($("#name-on-card").val() == "") {
            $("#name-on-card").val(GetCookie("Name"));
        }
        if ($("#email").val() == "") {
            $("#email").val(GetCookie("Email"));
        }
    }

    if ($("#country").data("preselected") == undefined) {
        var emsVisitorLocationTryCount = 0;
        var emsVisitorLocationTimer = setInterval(function(){
            emsVisitorLocationTryCount++;
            //Get visitor location
            var visitorLocationJSON;
            if (typeof GetCookie === "function") {
                var visitorLocation = GetCookie("visitor_location");
                if (visitorLocation != null && visitorLocation != "" && visitorLocation != undefined) {
                    try {
                        visitorLocationJSON = JSON.parse(visitorLocation);
                    } catch (e) {
                        console.log(e.message);
                        notifyErrorMessage($,e.message,notifyErrorSubject);
                    }
                }
            }

            if (emsVisitorLocationTryCount >= 5 || visitorLocationJSON != undefined) {
                clearInterval(emsVisitorLocationTimer);
                if (visitorLocationJSON == undefined) {
                    $.ajax({
                        url: ajax_object.form_functions,
                        type: 'GET',
                        data: {action: 'emsGetVisitorLocation'},
                        cache: false,
                        dataType: 'json',
                        success: function (jsonResponse) {
                            try {
                                if (jsonResponse.status == 'success') {
                                    setSelectedCountry($, jsonResponse);
                                    if (typeof SetCookieExp === "function") {
                                        delete jsonResponse["status"];
                                        SetCookieExp("visitor_location", JSON.stringify(jsonResponse), 30);
                                    }
                                }
                                else if (jsonResponse.status == 'error' || jsonResponse.status == 'fail') {
                                    console.log(jsonResponse.message);
                                }
                            } catch (e) {
                                console.log(e.message);
                                notifyErrorMessage($,e.message,notifyErrorSubject);
                            }
                        },
                        error: function (xhr) {
                            console.log(xhr.responseText);
                            notifyErrorMessage($,xhr.responseText,notifyErrorSubject);
                        }
                    });
                }
                else {
                    setSelectedCountry($, visitorLocationJSON);
                }
            }
            emsVisitorLocationTryCount++;
        }, 1000);
    }
});