/*
<div class="tags">
    <div class="tags-container">
        <span class="tag badge badge-secondary mr-1">Amsterdam<span data-role="remove"></span></span> 
        <input type="text" class="tags-panel" placeholder="add tags" size="8">
    </div>
    <input type="text" value="Amsterdam," data-role="tagsinput" class="tags-value" name="addresses" style="display: none;">
</div> 
*/
$("input.tags-panel").change(function(e) {
    if($(e.target).val() === '')
        return;
    $("<span class=\"tag badge badge-secondary mx-1\">" + $(e.target).val() +"<span data-role=\"remove\"></span></span>").insertBefore($(e.target));
    $(e.target).val("");
});


let addresses = [];
$("input.addresses-panel").change(function(e) {
    if($(e.target).val() === '')
        return;

    if(e.target.dataset.toggleEditForm) {
        $(e.target).parent().parent().next().removeClass('d-none');
    }

    $($(e.target).parent()).append("<li class=\"address bg-dark text-light d-flex list-group-item\"><div class=\"mr-auto\"><span class=\"fa fa-map mr-2\"></span>" + $(e.target).val() +"</div><span class=\"fa fa-close ml-auto mr-1 remove\" onclick=\"removeAddress(event)\"></span></li>")
    addresses.push( $(e.target).val());
    $($(e.target).parent()).parent().next().children('.addresses-value').val(addresses.toString());
    $(e.target).val("");
});
function removeAddress(e) {
    if(e === null)
        return;
        //delete addresses[addresses.indexOf($(e.target).prev().val())];
        addresses.splice(addresses.indexOf($(e.target).prev().val()), 1);
        $(e.target).parent().parent().next().val(addresses.toString());
        $(e.target).parent().remove();
}

$("input.custom-from-control").keyup(function(e) {
    if($(e.target).val() === "")
    {
        $(e.target).prev().text('');
    }
    else
    {
        $(e.target).prev().text(e.target.placeholder);
    }
});

$("input.account-detail-switcher").change(function(e) {
    let view = $(e.target).parent().parent().children(".account-detail-body");
    view.children('.editable').toggleClass('d-none');
});
$(".account-detail .account-detail-body .account-detail-edit-form .form-group button").click(function(e) {
    
    if($(e.target).hasClass('save'))
    {
        let parent = $(e.target).parent().parent().parent().parent();
        let spinner = $(parent).children('.pull-right').children('.d-none');
        spinner.removeClass('d-none');
        return;
    }
    
    if($(e.target).hasClass('cancel'))
    {
        let parent = $(e.target).parent().parent().parent().parent();
        let switcher = $(parent).children('.pull-right').children('.account-detail-switcher');
        $(switcher).click();
        return;
    }
});

function removePaymentMethod(e) {
    console.log(e);
}

function viewImageInModal(e, modal) {
    $(modal).children('.modal-dialog')
        .children('.modal-content')
        .children('img')
        .attr('src', $(e.target).attr('src'));
    $(modal).modal('toggle');
}

function onNewAttachmentButtonClicked (e) {
    $('<div class="col-md-3 col-xs-12 my-1" style="height:200px"><input type="file" class="dropify"></div>').insertBefore($('#add_new_attchment').parent());
    initializeDropifyItems()
}

function initializeDropifyItems() {
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'delete': 'Delete',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
}

function viewImageInModal(e, modal) {
    let child = $(modal).children('.modal-dialog').children('.modal-content').children('img')
    child.attr('src', $(e.target).attr('src'));
    child.removeClass('d-none');
    $(modal).modal('toggle');
}

function viewPDFInModal(e, modal) {
    let  child = $(modal).children('.modal-dialog').children('.modal-content').children('p');
    child.text('The plan was to do only one trailer for our kids high school band. Now, high schools around the country want one of our trailers! We now have a two-year waiting list for our trailers, and ALL machined parts come from eMachineShop');
    child.removeClass('d-none');
    $(modal).modal('toggle');
}

function hideModalChilren(modal) {
    $(modal).children('.modal-dialog').children('.modal-content').children().addClass('d-none');
}