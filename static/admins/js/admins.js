function initDropify() {
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
}

function initialaize_dragula(element1, element2, element3) 
{
    dragula([document.querySelector(element1), document.querySelector(element2), document.querySelector(element3)], {
        isContainer: function (el) {
            return false; // only elements in drake.containers will be taken into account
        },
        moves: function (el, source, handle, sibling) {
            return true; // elements are always draggable by default
        },
        accepts: function (el, target, source, sibling) {

            return true; // elements can be dropped in any of the `containers` by default
        },
        invalid: function (el, handle) {

            return false; // don't prevent any drags from initiating by default
        },
        direction: 'vertical',             // Y axis is considered when determining where an element would be dropped
        copy: false,                       // elements are moved by default, not copied
        copySortSource: false,             // elements in copy-source containers can be reordered
        revertOnSpill: false,              // spilling will put the element back where it was dragged from, if this is true
        removeOnSpill: false,              // spilling will `.remove` the element, if this is true
        mirrorContainer: document.body,    // set the element that gets mirror elements appended
        ignoreInputTextSelection: true     // allows users to select input text, see details below
    });
}

function showNext(e) {
    console.log($(e.target).parent().next());
    $(e.target).parent().next().fadeIn(1111);
    $(e.target).parent().next().children().toggleClass('d-none');
}

function maximize(sender) {
    let parent = $(sender).data('parent');
    console.log(parent);
    $(parent).removeClass('col-xl-4');
    $(parent).addClass('col-xl-8');
}

function minimize(sender) {
    let parent = $(sender).data('parent');
    console.log(parent);
    $(parent).removeClass('col-xl-8');
    $(parent).addClass('col-xl-4');
}

function fixLeftNavDimensions() {
    $('.side-menu').css('top', 0);
    $('#topnav').css('marginLeft', $('.side-menu').css('width'));
    $('#wrapper-content').css('marginLeft', $('.side-menu').css('width'));
}

function toggleSideBarMenu(sidebar) {
    $(sidebar + ' ul li a span').toggleClass('d-none');
    $(sidebar + ' ul li button span').toggleClass('d-none');
    
    fixLeftNavDimensions();
    //$('#content').css('margin-left', $('#content').css('margin-left') === '250px' ? '75px' : '250px');
}

function viewImageInModal(e, modal) {
    let child = $(modal).children('.modal-dialog').children('.modal-content').children('img')
    child.attr('src', $(e.target).attr('src'));
    child.removeClass('d-none');
    $(modal).modal('toggle');
}

function viewPDFInModal(e, modal) {
    let  child = $(modal).children('.modal-dialog').children('.modal-content').children('p');
    child.text('The plan was to do only one trailer for our kids high school band. Now, high schools around the country want one of our trailers! We now have a two-year waiting list for our trailers, and ALL machined parts come from eMachineShop');
    child.removeClass('d-none');
    $(modal).modal('toggle');
}

function hideModalChilren(modal) {
    $(modal).children('.modal-dialog').children('.modal-content').children().addClass('d-none');
}